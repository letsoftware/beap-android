package com.project.bag.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.bag.R;
import com.project.client_library.ui.CongratulationScreen;


public class CongratulationScreenBAG extends CongratulationScreen {
    private ImageView bgImg;
    private TextView msg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bgImg =  (ImageView)findViewById(R.id.mainImg);
        msg =  (TextView)findViewById(R.id.msg);

        String getMessage = getIntent().getStringExtra("msg");
        if(getMessage.length()!=0){
            msg.setVisibility(View.VISIBLE);
            msg.setText(getMessage);
        }



        bgImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getIntent().getIntExtra("status",0)==1){
                    Intent i= new Intent(CongratulationScreenBAG.this,WelcomeScreenBAG.class);
                    startActivity(i);
                    finish();
                }else{
                    Intent i= new Intent(CongratulationScreenBAG.this,HomeScreenBAG.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("new_user",true);
                    startActivity(i);
                    finish();
                }

            }
        });


    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_congratulationscreen;
    }

    @Override
    protected void onResume() {
        super.onResume();



    }



    //end of main class
}
