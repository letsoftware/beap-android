package com.project.bag.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.project.client_library.Cells.CellStandard;
import com.project.client_library.Cells.CellButton;
import com.project.client_library.helpers.AeSimpleSHA1;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.helpers.Validations;
import com.project.client_library.interfaces.CheckButton;
import com.project.client_library.interfaces.POSTResponseListener;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.User;
import com.project.client_library.model.UserModel;
import com.project.client_library.ui.CreateUserScreen;
import com.project.client_library.widget.AlertBox;
import com.project.client_library.widget.HUD;
import com.project.bag.R;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;

import helpers.AppConstantsBAG;

import static com.project.client_library.helpers.CheckInternet.CheckInternetConnection;


public class CreateUserScreenBAG extends CreateUserScreen {

    private Toolbar toolbar;
    private TextView toolbarTitleTextView;
    private ViewGroup footer;
    private Button footerButton;
    private User userObject;
    private JSONObject userJSONObject;
    private HUD dialog;
    private ListView listViewCreateUser;
    private ArrayList<CheckButton> createScreenCellModelArrayList;
    private boolean isProfileEditable;
    private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("CreateUserScreenBAG", "onCreate called");

        //check profile is editable or not
        isProfileEditable = getIntent().getBooleanExtra("isEditProfile", false);

        listViewCreateUser = (ListView) findViewById(R.id.listviewCreateUser);

        createScreenCellModelArrayList = new ArrayList<>();

        footer = setAdapter(CreateUserScreenBAG.this, createScreenCellModelArrayList, listViewCreateUser);
        footerButton = (Button) footer.findViewById(R.id.btnCreateUser);

        setToolbar();

        userObject = new User();

        if(isProfileEditable) {

            toolbarTitleTextView.setText("Edit User");
            footerButton.setText("Save");

            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenBAG.this, "user_pref", 0);
            userModel = complexPreferences.getObject("current-user", UserModel.class);
            Log.e("CreateUserScreenDAC", "onCreate if isProfileEditable then userModel retrieved from preferences is: userId: " + userModel.userId + " userPassword: " + userModel.User.userEncryptedPassword);

            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CreateUserScreenBAG.this.CONNECTIVITY_SERVICE);
            if(CheckInternetConnection(connectivityManager)) {
                processFetchUserDetails();
            }
        } else {

            toolbarTitleTextView.setText(getString(R.string.creatuser_btn));

            initAdapter(null);
        }
    }

    private void initAdapter(UserModel tempUserModel) {

        Log.e("CreateUserScreenBAG", "initAdapter called");

        initValue(tempUserModel);

        footerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isValid = checkValidation();
                if (isValid) {
                    process(userObject);
                }
            }
        });
    }

    private void initView() {

        Log.e("CreateUserScreenBAG", "initView called");

        listViewCreateUser = (ListView) findViewById(R.id.listviewCreateUser);

        ViewGroup footer = setAdapter(CreateUserScreenBAG.this, createScreenCellModelArrayList, listViewCreateUser);
        footerButton = (Button) footer.findViewById(R.id.btnCreateUser);
    }

    private void setToolbar() {

        Log.e("CreateUserScreenDAC", "setToolbar called");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitleTextView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_blue);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initValue(UserModel tempUserModel) {

        Log.e("CreateUserScreenDAC", "initValue called");

        createScreenCellModelArrayList.clear();

        if (tempUserModel == null) {
            if (isProfileEditable) {
                ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenBAG.this, "user_pref", 0);
                userModel = complexPreferences.getObject("current-user", UserModel.class);
                // istop  ismiddle  isbottom hintValue defaultValue
                createScreenCellModelArrayList.add(new CellStandard(true, false, false, textType, getResources().getString(R.string.createuser_hint1), userModel.User.name));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint2), userModel.User.userMail));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint3), userModel.User.userMail));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, passwordType, getResources().getString(R.string.createuser_hint4), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, passwordType, getResources().getString(R.string.createuser_hint5), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, false, true, textType, getResources().getString(R.string.createuser_hint6), userModel.User.userBirthday));

                // isradio  isswitch  gender(1/2) newslater(0/1)
                createScreenCellModelArrayList.add(new CellButton(true, false, userModel.User.userGender, userModel.User.userNewsletter));
                createScreenCellModelArrayList.add(new CellButton(false, true, userModel.User.userGender, userModel.User.userNewsletter));

                createScreenCellModelArrayList.add(new CellStandard(true, false, false, textType, getResources().getString(R.string.createuser_hint7), userModel.User.userCar_brand));
                createScreenCellModelArrayList.add(new CellStandard(false, false, true, textType, getResources().getString(R.string.createuser_hint8), userModel.User.userCar_registration_number + ""));
            } else {
                // istop  ismiddle  isbottom hintValue defaultValue
                createScreenCellModelArrayList.add(new CellStandard(true, false, false, textType, getResources().getString(R.string.createuser_hint1), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint2), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint3), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, passwordType, getResources().getString(R.string.createuser_hint4), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, passwordType, getResources().getString(R.string.createuser_hint5), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, false, true, textType, getResources().getString(R.string.createuser_hint6), ""));

                // isradio  isswitch  gender(1/2) newslater(0/1)
                createScreenCellModelArrayList.add(new CellButton(true, false, 1, 0));
                createScreenCellModelArrayList.add(new CellButton(false, true, 1, 0));

                createScreenCellModelArrayList.add(new CellStandard(true, false, false, textType, getResources().getString(R.string.createuser_hint7), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, false, true, textType, getResources().getString(R.string.createuser_hint8), ""));
            }
        }
        else {
            // istop  ismiddle  isbottom hintValue defaultValue
            createScreenCellModelArrayList.add(new CellStandard(true, false, false, textType, getResources().getString(R.string.createuser_hint1), tempUserModel.User.name));
            createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint2), tempUserModel.User.userMail));
            createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint3), tempUserModel.User.userMail));
            createScreenCellModelArrayList.add(new CellStandard(false, true, false, passwordType, getResources().getString(R.string.createuser_hint4), ""));
            createScreenCellModelArrayList.add(new CellStandard(false, true, false, passwordType, getResources().getString(R.string.createuser_hint5), ""));
            createScreenCellModelArrayList.add(new CellStandard(false, false, true, textType, getResources().getString(R.string.createuser_hint6), tempUserModel.User.userBirthday));

            // isradio  isswitch  gender(1/2) newslater(0/1)
            createScreenCellModelArrayList.add(new CellButton(true, false, userModel.User.userGender, tempUserModel.User.userNewsletter));
            createScreenCellModelArrayList.add(new CellButton(false, true, userModel.User.userGender, tempUserModel.User.userNewsletter));

            createScreenCellModelArrayList.add(new CellStandard(true, false, false, textType, getResources().getString(R.string.createuser_hint7), tempUserModel.User.userCar_brand));
            createScreenCellModelArrayList.add(new CellStandard(false, false, true, textType, getResources().getString(R.string.createuser_hint8), tempUserModel.User.userCar_registration_number + ""));
        }

        updateAdapter();
    }

    private boolean checkValidation() {

        Log.e("CreateUserScreenBAG", "checkValidation called");

        boolean value = false;
        for (int i = 0; i < listViewCreateUser.getAdapter().getCount() - 1; i++) {

            if (createScreenCellModelArrayList.get(i).isRadioButton()) {
                CellButton cellButton = (CellButton) createScreenCellModelArrayList.get(i);
                userObject.userGender = cellButton.radioValue;
            }
            else if (createScreenCellModelArrayList.get(i).isSwitchButton()) {
                CellButton cellButton = (CellButton) createScreenCellModelArrayList.get(i);
                userObject.userNewsletter = cellButton.switchValue;
            }
            else {
                CellStandard cellStandard = (CellStandard) createScreenCellModelArrayList.get(i);

                if (cellStandard.inputType == textType) {
                    if (Validations.isEmptyField(cellStandard.etValue)) {
                        // TODO proper error handling
                        Toast.makeText(CreateUserScreenBAG.this, getString(R.string.code_create_user_PLEASEENTER)  + cellStandard.hintValue, Toast.LENGTH_LONG).show();
                        return false;
                    }
                    else {
                        value = true;
                    }
                }
                else if (cellStandard.inputType == emailType) {
                    if (Validations.isEmptyField(cellStandard.etValue)) {
                        // TODO proper error handling
                        Toast.makeText(CreateUserScreenBAG.this, getString(R.string.code_create_user_PLEASEENTER) + cellStandard.hintValue, Toast.LENGTH_LONG).show();
                        return false;
                    }
                    else if (!Validations.isEmailPattern(cellStandard.etValue)) {
                        // TODO proper error handling
                        Toast.makeText(CreateUserScreenBAG.this, getString(R.string.code_createuser_ENTEREMAIL), Toast.LENGTH_LONG).show();
                        return false;
                    }
                    else {
                        value = true;
                    }

                    if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.confirm_email)) && cellStandard.etValue.length() != 0) {
                        CellStandard cellTemp = (CellStandard) createScreenCellModelArrayList.get(i - 1);
                        String tempValue = cellTemp.etValue;
                        Log.e("CreateUserScreenBAG", "checkValidation email cell tempValue: " + tempValue + " cellStandard.etValue: " + cellStandard.etValue);
                        if (!Validations.isEmailMatch(cellStandard.etValue, tempValue)) {
                            // TODO proper error handling
                            Toast.makeText(CreateUserScreenBAG.this, getString(R.string.code_EMAILDONOTMATCH), Toast.LENGTH_LONG).show();
                            return false;
                        }
                        else {
                            value = true;
                        }
                    }

                } else if (cellStandard.inputType == passwordType) {
                    if (Validations.isEmptyField(cellStandard.etValue)) {
                        // TODO proper error handling
                        Toast.makeText(CreateUserScreenBAG.this, getString(R.string.code_create_user_PLEASEENTER) + cellStandard.hintValue, Toast.LENGTH_LONG).show();
                        return false;
                    }
                    else {
                        value = true;
                    }

                    if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.confirm_password)) && cellStandard.etValue.length() != 0) {
                        CellStandard cellTemp = (CellStandard) createScreenCellModelArrayList.get(i - 1);
                        String tempValue = cellTemp.etValue;
                        Log.e("CreateUserScreenBAG", "checkValidation password cell tempValue: " + tempValue + " cellStandard.etValue: " + cellStandard.etValue);
                        if (!Validations.isEmailMatch(cellStandard.etValue, tempValue)) {
                            // TODO proper error handling
                            Toast.makeText(CreateUserScreenBAG.this, getString(R.string.code_PASSWORDDONOTMATCH), Toast.LENGTH_LONG).show();
                            return false;
                        }
                        else {
                            value = true;
                        }
                    }
                }

                if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.name))) {
                    userObject.name = cellStandard.etValue;
                }
                else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.email))) {
                    userObject.userMail = cellStandard.etValue;
                }
                else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.password))) {
                    String encrypted = "";
                    try {
                        encrypted = AeSimpleSHA1.SHA1(cellStandard.etValue.trim());
                    }
                    catch (NoSuchAlgorithmException e) {
                        Log.e("CreateUserScreenBAG", "execption in encryption: " + e.toString());
                        e.printStackTrace();
                    }
                    catch (UnsupportedEncodingException e) {
                        Log.e("CreateUserScreenBAG", "execption in encryption: " + e.toString());
                        e.printStackTrace();
                    }
                    userObject.userEncryptedPassword = encrypted;
                }
                else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.birthday))) {
                    userObject.userBirthday = cellStandard.etValue;
                }
                else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.car_make))) {
                    userObject.userCar_brand = cellStandard.etValue;
                }
                else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.car_regno))) {
                    userObject.userCar_registration_number = cellStandard.etValue;
                }
            }
        }

        Log.e("CreateUserScreenBAG", "checkValidation returnning: " + value);
        return value;
    }

    private void process(User userObject) {

        Log.e("CreateUserScreenBAG", "process called with userObject: " + userObject.toString());

        try {
            userJSONObject = new JSONObject();
            userJSONObject.put("name", userObject.name.toString().trim());
            userJSONObject.put("mail", userObject.userMail.toString().trim());
            userJSONObject.put("password", userObject.userEncryptedPassword); //sha1 of the password typed in the text edit
            userJSONObject.put("newsletter", String.valueOf(userObject.userNewsletter));
            userJSONObject.put("birthday", userObject.userBirthday); //string in format "01-01-1970"
            userJSONObject.put("gender", String.valueOf(userObject.userGender));
            userJSONObject.put("car_brand", userObject.userCar_brand.toString().trim());
            userJSONObject.put("car_registration_number", userObject.userCar_registration_number.toString().trim());
        }
        catch (Exception e){
            Log.e("CreateUserScreenBAG", "process exception: " + e.toString());
            e.printStackTrace();
        }

        if(isProfileEditable){
            processUpdateUser(userJSONObject,userObject);
        } else {
            processCreateUser(userJSONObject);
        }
    }

    private void processUpdateUser(JSONObject JSONUserObject, User userObject) {

        Log.e("CreateUserScreenBAG", "processUpdateUser called with JSONUserObject: " + JSONUserObject.toString() + " and JSONUserObject: " + userObject.toString());

        AppConstants obj= new AppConstants();

        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsBAG.PROJECT_BAG,lang, AppConstantsBAG.CREATE_USER+"/"+userObject.uid);

        RequestJSON json = new RequestJSON();
        json.POST(CreateUserScreenBAG.this, LINK, JSONUserObject.toString());
        json.setPostResponseListener(new POSTResponseListener() {
            @Override
            public String onPost(String msg) {

                Log.e("CongratulationScreenBAG", "onPost response: " + msg);

                try {
                    JSONObject jsonObject = new JSONObject(msg.toString().trim());
                    Iterator<?> keys = jsonObject.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        if (key.toString().trim().equals("error")) {
                            AlertBox.showBox(CreateUserScreenBAG.this, getString(R.string.ERROR), jsonObject.getString("error").toString().trim());
                        }
                        else {
                            UserModel userModelObject = new GsonBuilder().create().fromJson(msg, UserModel.class);
                            Log.e("CreateUserScreenBAG", "onPost saving userModelObject in preferences: " + userModelObject.toString());
                            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenBAG.this, "user_pref", 0);
                            complexPreferences.putObject("current-user", userModelObject);
                            complexPreferences.commit();

                            Log.e("CongratulationScreenBAG", "onPost creating new CongratulationScreenBAG activity");
                            Intent i = new Intent(CreateUserScreenBAG.this, CongratulationScreenBAG.class);
                            i.putExtra("msg", getString(R.string.USER_UPDATE));
                            startActivity(i);
                            finish();
                        }
                    }
                }
                catch (Exception e) {
                    Log.e("CreateUserScreenBAG", "onPost response exception: " + e.toString());
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public void onPreExecute() {

            }

            @Override
            public void onBackground() {

            }
        });
    }


    private void processCreateUser(JSONObject userJSONObject) {

        Log.e("CreateUserScreenBAG", "processCreateUser called with JSONUserObject: " + this.userJSONObject.toString());

        AppConstants obj= new AppConstants();
        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");


        String LINK = obj.setURL(AppConstantsBAG.PROJECT_BAG, lang, AppConstantsBAG.CREATE_USER);

        RequestJSON json = new RequestJSON();
        json.POST(CreateUserScreenBAG.this, LINK, userJSONObject.toString());
        json.setPostResponseListener(new POSTResponseListener() {
            @Override
            public String onPost(String msg) {

                Log.e("CreateUserScreenBAG", "onPost response: " + msg);

                try {
                    JSONObject jsonObject = new JSONObject(msg.toString().trim());
                    Iterator<?> keys = jsonObject.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        if (key.toString().trim().equals("error")) {
                            AlertBox.showBox(CreateUserScreenBAG.this, getString(R.string.ERROR), jsonObject.getString("error").toString().trim());
                        } else {
                            UserModel userModelObject = new GsonBuilder().create().fromJson(msg, UserModel.class);
                            Log.e("CreateUserScreenBAG", "onPost saving userModelObject in preferences: " + userModelObject.toString());
                            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenBAG.this, "user_pref", 0);
                            complexPreferences.putObject("current-user", userModelObject);
                            complexPreferences.commit();

                            Log.e("CreateUserScreenBAG", "onPost saving isUserLogin in preferences: " + true);
                            SharedPreferences preferences = getSharedPreferences("login", MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean("isUserLogin", true);
                            editor.commit();

                            Log.e("CreateUserScreenBAG", "onPost creating new CongratulationScreenDAC activity");
                            Intent i = new Intent(CreateUserScreenBAG.this, CongratulationScreenBAG.class);
                            i.putExtra("msg", getString(R.string.congo_msg));
                            startActivity(i);
                            finish();
                        }
                    }
                } catch (Exception e) {
                    Log.e("excepp", e.toString());
                }
                return null;
            }

            @Override
            public void onPreExecute() {

            }

            @Override
            public void onBackground() {

            }
        });

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_createuserscreen;
    }

    private void processFetchUserDetails() {

        Log.e("CreateUserScreenBAG", "processFetchUserDetails called");

        dialog = new HUD(CreateUserScreenBAG.this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.title("");
        dialog.show();

        RequestJSON json = new RequestJSON();
        AppConstants obj = new AppConstants();

        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");



        String LINK = obj.setURL(AppConstantsBAG.PROJECT_BAG,lang, AppConstantsBAG.CREATE_USER+"/"+userModel.userId);
        json.GET_METHOD(LINK);

        json.setResponseListener(new ResponseListener() {
            @Override
            public String response(String response) {

                Log.e("CreateUserScreenBAG", "onPost response: " + response);

                dialog.dismiss();
                UserModel tempUserModel = new GsonBuilder().create().fromJson(response, UserModel.class);
                /*
                Log.e("CreateUserScreenBAG", "onPost saving userModelObject in preferences: " + userModel.toString());
                ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenBAG.this, "user_pref", 0);
                complexPreferences.putObject("current-user", userModel);
                complexPreferences.commit();
                */
                initAdapter(tempUserModel);

                return null;
            }

            @Override
            public String error(String error) {
                Log.e("CreateUserScreenBAG", "GET response exception: " + error);
                return null;
            }
        });

    }

//    private void setUserValue() {
//        for (int i = 0; i < listViewCreateUser.getAdapter().getCount() - 1; i++) {
//
//            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenBAG.this, "user_pref", 0);
//            UserModel modelUsr=complexPreferences.getObject("current-user", UserModel.class);
//            User tempUSer=modelUsr.User;
//            if (createScreenCellModelArrayList.get(i).isRadioButton()) {
//                View vListSortOrder;
//                vListSortOrder = listViewCreateUser.getChildAt(i);
//               final CellButton cellButton = (CellButton) createScreenCellModelArrayList.get(i);
//                SegmentedGroup segmentGroup = (SegmentedGroup) vListSortOrder.findViewById(com.project.client_library.R.id.segmentedGroup);
//
////                RadioButton radioButtonMale = (RadioButton)segmentGroup.findViewById(com.project.client_library.R.id.rbMand);
////                RadioButton radioButtonFeMale = (RadioButton)segmentGroup.findViewById(com.project.client_library.R.id.rbKvinde);
//
//
//
////                Toast.makeText(CreateUserScreenBAG.this,segmentGroup.getCheckedRadioButtonId()+" female",Toast.LENGTH_LONG).show();
//                if(tempUSer.userGender==1){
////                    segmentGroup.(com.project.client_library.R.id.rbMand);
////                    Toast.makeText(CreateUserScreenBAG.this,tempUSer.userGender+" male",Toast.LENGTH_LONG).show();
//
//                } else {
//                    segmentGroup.check(com.project.client_library.R.id.rbKvinde);
////                    Toast.makeText(CreateUserScreenBAG.this,tempUSer.userGender+" female",Toast.LENGTH_LONG).show();
//
//                }
//
//                 cellButton.radioValue=tempUSer.userGender;
//
//            } else if (createScreenCellModelArrayList.get(i).isSwitchButton()) {
//                View vListSortOrder;
//                vListSortOrder = listViewCreateUser.getChildAt(i);
//                CellButton cellButton = (CellButton) createScreenCellModelArrayList.get(i);
//                Switch switchView = (Switch) vListSortOrder.findViewById(com.project.client_library.R.id.switchView);
//                switchView.setChecked(true);
//                switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                        Toast.makeText(CreateUserScreenBAG.this,"click",Toast.LENGTH_LONG).show();
//                    }
//                });
////                CreateUserAdapter createUserAdapter=(CreateUserAdapter)listViewCreateUser.getAdapter();
////                createUserAdapter.notifyDataSetInvalidated();
////                if(tempUSer.userNewsletter==0){
////                    switchView.setChecked(false);
////                } else {
////                    switchView.setChecked(true);
////                }
//
//                 cellButton.switchValue=tempUSer.userNewsletter;
//
//            } else {
//                View vListSortOrder;
//                vListSortOrder = listViewCreateUser.getChildAt(i);
//
//                CellStandard cellStandard = (CellStandard) createScreenCellModelArrayList.get(i);
////                EditText editText= (EditText) vListSortOrder.findViewById(R.id.etField);
//                if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.name))) {
//                     cellStandard.etValue=tempUSer.name;
////                    editText.setText(cellStandard.etValue);
//                } else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.email))) {
//                    cellStandard.etValue=tempUSer.userMail;
////                    editText.setText(cellStandard.etValue);
//                }else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.confirm_email))) {
//                    cellStandard.etValue=tempUSer.userMail;
////                    editText.setText(cellStandard.etValue);
//                } else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.birthday))) {
//                    cellStandard.etValue=tempUSer.userBirthday;
////                    editText.setText(cellStandard.etValue);
//                }
//                else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.car_make))) {
//                    cellStandard.etValue=tempUSer.userCar_brand;
////                    editText.setText(cellStandard.etValue);
//                }
//                else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.car_regno))) {
//                   cellStandard.etValue=tempUSer.userCar_registration_number;
////                    editText.setText(cellStandard.etValue);
//                }
//            }
//
//        }
//        listViewCreateUser.invalidateViews();
//    }
}