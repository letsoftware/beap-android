package com.project.bag.ui;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.WindowManager;
import android.widget.TabHost;
import android.widget.TextView;

import com.project.bag.R;
import com.project.client_library.ui.HomeScreen;
import com.project.client_library.widget.WMImageView;

import fragments.AboutUsFragment;
import fragments.HomeFragment;
import fragments.MyAreaFragment;
import fragments.FavouriteFragment;
import fragments.SignalFragment;


public class HomeScreenBAG extends HomeScreen {

    private FragmentTabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initTabHost();

        SharedPreferences preferences = getSharedPreferences("lock", MODE_PRIVATE);
        if(preferences.getBoolean("isLock",false)){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        if(getIntent().getBooleanExtra("new_user", false)){
            mTabHost.setCurrentTab(1);
            selectTAB(1);
        } else {
            mTabHost.setCurrentTab(0);
            selectTAB(0);
        }

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                for(int i=0;i<mTabHost.getTabWidget().getTabCount();i++){
                    selectTAB(i);
                }
            }
        });

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_myareascreen;
    }


    private void initTabHost() {
    /*
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(PlayTabActivity.this, "user_pref", 0);
        user = complexPreferences.getObject("current_user", User.class);
    */

        mTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

        View view1 = getLayoutInflater().inflate(R.layout.item_tab_menu,null,false);
        View view2 = getLayoutInflater().inflate(R.layout.item_tab_myarea,null,false);
        View view3 = getLayoutInflater().inflate(R.layout.item_tab_fav,null,false);
        View view4 = getLayoutInflater().inflate(R.layout.item_tab_info,null,false);
        View view5 = getLayoutInflater().inflate(R.layout.item_tab_signal,null,false);

        TextView tv1=(TextView)view1.findViewById(R.id.txtTab);
        tv1.setTextColor(getResources().getColor(R.color.myarea_text));

        TextView tv2=(TextView)view2.findViewById(R.id.txtTab);
        tv2.setTextColor(getResources().getColor(R.color.myarea_text));

        TextView tv3=(TextView)view3.findViewById(R.id.txtTab);
        tv3.setTextColor(getResources().getColor(R.color.myarea_text));

        TextView tv4=(TextView)view4.findViewById(R.id.txtTab);
        tv4.setTextColor(getResources().getColor(R.color.myarea_text));

        TextView tv5=(TextView)view5.findViewById(R.id.txtTab);
        tv5.setTextColor(getResources().getColor(R.color.myarea_text));


        View v1=view1.findViewById(R.id.topTab);
        v1.setBackgroundColor(getResources().getColor(R.color.radio_button_selected_color));

        View v2=view2.findViewById(R.id.topTab);
        v2.setBackgroundColor(getResources().getColor(R.color.radio_button_selected_color));

        View v3=view3.findViewById(R.id.topTab);
        v3.setBackgroundColor(getResources().getColor(R.color.radio_button_selected_color));

        View v4=view4.findViewById(R.id.topTab);
        v4.setBackgroundColor(getResources().getColor(R.color.radio_button_selected_color));

        View v5=view5.findViewById(R.id.topTab);
        v5.setBackgroundColor(getResources().getColor(R.color.radio_button_selected_color));


        mTabHost.addTab(mTabHost.newTabSpec("tab1").setIndicator(view1),
                HomeFragment.class, null);

        mTabHost.addTab(mTabHost.newTabSpec("tab2").setIndicator(view2),
                MyAreaFragment.class, null);

        mTabHost.addTab(mTabHost.newTabSpec("tab3").setIndicator(view3),
                FavouriteFragment.class, null);

        mTabHost.addTab(mTabHost.newTabSpec("tab4").setIndicator(view4),
                AboutUsFragment.class, null);

        mTabHost.addTab(mTabHost.newTabSpec("tab5").setIndicator(view5),
                SignalFragment.class, null);

    }


    @Override
    public void onBackPressed() {
        Fragment tab1 = (HomeFragment)getSupportFragmentManager().findFragmentByTag("tab1");
        Fragment tab2 = (MyAreaFragment)getSupportFragmentManager().findFragmentByTag("tab2");
        Fragment tab3 = (FavouriteFragment)getSupportFragmentManager().findFragmentByTag("tab3");
        Fragment tab4 = (AboutUsFragment)getSupportFragmentManager().findFragmentByTag("tab4");
        Fragment tab5 = (SignalFragment)getSupportFragmentManager().findFragmentByTag("tab5");

        if(tab1 != null && tab1 instanceof HomeFragment) {
            try {
                super.onBackPressed();
            } catch (Exception e){
                HomeFragment hom = new HomeFragment();
                FragmentManager fragmentManager =  getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.realtabcontent, hom);
                fragmentTransaction.commit();
            }
        } else if(tab2 != null && tab2 instanceof MyAreaFragment) {
            try {
                super.onBackPressed();
            } catch (Exception e){
                MyAreaFragment myarea = new MyAreaFragment();
                FragmentManager fragmentManager =  getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.realtabcontent, myarea);
                fragmentTransaction.commit();
            }
        } else if(tab3 != null && tab3 instanceof FavouriteFragment) {
            try {
                super.onBackPressed();
            } catch (Exception e){
                FavouriteFragment fav = new FavouriteFragment();
                FragmentManager fragmentManager =  getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.realtabcontent, fav);
                fragmentTransaction.commit();
            }
        } else if(tab4 != null && tab4 instanceof AboutUsFragment) {
            try {
                super.onBackPressed();
            } catch (Exception e){
                AboutUsFragment about = new AboutUsFragment();
                FragmentManager fragmentManager =  getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.realtabcontent, about);
                fragmentTransaction.commit();
            }
        } else if(tab5 != null && tab5 instanceof SignalFragment) {
            try {
                super.onBackPressed();
            } catch (Exception e){
                SignalFragment signal = new SignalFragment();
                FragmentManager fragmentManager =  getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.realtabcontent, signal);
                fragmentTransaction.commit();
            }
        } else{
            super.onBackPressed();
        }
    }


    private void selectTAB(int i) {

        View v = mTabHost.getTabWidget().getChildAt(i);
        WMImageView iv = (WMImageView)v.findViewById(R.id.imgTab);
        TextView tv=(TextView)v.findViewById(R.id.txtTab);
        View topLine = v.findViewById(R.id.topTab);

        if(i==mTabHost.getCurrentTab()) {
            iv.selected(getResources().getColor(R.color.radio_button_selected_color));
            v.setBackgroundColor(Color.WHITE); // setting the theme color of tab bar
            tv.setTextColor(getResources().getColor(R.color.radio_button_selected_color));
            topLine.setVisibility(View.VISIBLE);
        }
        else {
            iv.normal(getResources().getColor(R.color.myarea_image));
            v.setBackgroundColor(getResources().getColor(R.color.tabbar_background_color));
            tv.setTextColor(getResources().getColor(R.color.myarea_text));
            topLine.setVisibility(View.INVISIBLE);
        }
    }
}