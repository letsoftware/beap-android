package com.project.bag.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.project.bag.R;
import com.project.client_library.helpers.AeSimpleSHA1;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.POSTResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.UserModel;
import com.project.client_library.ui.LoginScreen;
import com.project.client_library.widget.AlertBox;
import com.project.client_library.widget.HUD;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import helpers.AppConstantsBAG;


public class LoginScreenBAG extends LoginScreen implements View.OnClickListener{
    private Toolbar toolbar;
    private TextView txtCreateUser,txtForgotPass;
    private Button btnLogin;
    private EditText etEmail,etPassword;
    private String encryptedPassword,decryptedPassword;
    private HUD dialog;
    private JSONObject userObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_blue);
        toolbar_Title.setText(getString(R.string.LOGIN));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        intView();

    }


    private void intView(){
        txtCreateUser = (TextView)findViewById(R.id.txtCreateUser);
        txtForgotPass = (TextView)findViewById(R.id.txtForgotPass);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);



        btnLogin.setOnClickListener(this);
        txtCreateUser.setOnClickListener(this);
        txtForgotPass.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void processPasswordEncryption() {

        new AsyncTask<Void,Void,Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new HUD(LoginScreenBAG.this,android.R.style.Theme_Translucent_NoTitleBar);
                dialog.title("");
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    encryptedPassword = AeSimpleSHA1.SHA1(etPassword.getText().toString().trim());
                }
                catch (NoSuchAlgorithmException e) {
                    Log.e("exc in encyption", e.toString());
                }
                catch (UnsupportedEncodingException e) {
                    Log.e("exc in encyption", e.toString());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                dialog.dismiss();
                processLogin();
            }
        }.execute();

    }

    private void processLogin() {
        try {
            userObject = new JSONObject();
            userObject.put("mail", etEmail.getText().toString().trim());
            userObject.put("password", encryptedPassword); //sha1 of the password typed in the text edit

        }
        catch (Exception e){
            Log.e("exc",e.toString());
        }
        processResponse();
    }

    private void processResponse(){

        AppConstants obj= new AppConstants();

        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsBAG.PROJECT_BAG, lang, AppConstantsBAG.LOGIN);

        RequestJSON json = new RequestJSON();
        json.POST(LoginScreenBAG.this, LINK, userObject.toString());

        json.setPostResponseListener(new POSTResponseListener() {
            @Override
            public String onPost(final String msg) {
                Log.e("main response", msg);


                try {

                    JSONObject jobj = new JSONObject(msg.toString().trim());
                    Iterator<?> keys = jobj.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();

                        if (key.toString().trim().equals("error")) {

                            AlertBox.showBox(LoginScreenBAG.this, getString(R.string.LOGIN), jobj.getString("error").toString().trim());

                        } else {

                            UserModel userobj = new GsonBuilder().create().fromJson(msg, UserModel.class);
                            userobj.User.userEncryptedPassword = encryptedPassword;
                            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(LoginScreenBAG.this, "user_pref", 0);
                            complexPreferences.putObject("current-user", userobj);
                            complexPreferences.commit();


                            SharedPreferences preferences = getSharedPreferences("login", MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean("isUserLogin", true);
                            // editor.putString("userId ",edLoginPassword.getText().toString().trim());
                            editor.commit();

                            Intent i = new Intent(LoginScreenBAG.this, HomeScreenBAG.class);
                            startActivity(i);
                            finish();
                        }
                    }

                } catch (Exception e) {
                    Log.e("excepp", e.toString());
                }
                return null;
            }

            @Override
            public void onPreExecute() {

            }

            @Override
            public void onBackground() {

            }
        });

    }









    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtCreateUser:
                Intent i = new Intent(LoginScreenBAG.this, CreateUserScreenBAG.class);
                startActivity(i);
                break;
            case R.id.txtForgotPass:
                Intent i2 = new Intent(LoginScreenBAG.this, ForgotPasswordBAG.class);
                startActivity(i2);
                break;
            case R.id.btnLogin:
                if (isEmptyField(etEmail)) {
                    Toast.makeText(LoginScreenBAG.this, getString(R.string.ENTER_EMAIL), Toast.LENGTH_LONG).show();
                }else if (isEmptyField(etEmail))  {
                    Toast.makeText(LoginScreenBAG.this,getString(R.string.ENTER_PASSWORD),Toast.LENGTH_LONG).show();
                }else{
                    processPasswordEncryption();
                }
               break;
        }
    }

    private boolean isEmptyField(EditText param1) {
        boolean isEmpty = false;
        if (param1.getText() == null || param1.getText().toString().equalsIgnoreCase("")) {
            isEmpty = true;
        }
        return isEmpty;
    }

    //end of main class
}
