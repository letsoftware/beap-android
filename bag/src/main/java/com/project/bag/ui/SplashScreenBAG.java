package com.project.bag.ui;


import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.CountDownTimer;

import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.project.bag.R;
import com.project.client_library.helpers.AppConstants;

import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.Guide;


import java.util.Locale;

import helpers.AppConstantsBAG;

import static com.project.client_library.helpers.CheckInternet.CheckInternetConnection;


public class SplashScreenBAG extends ActionBarActivity {
    private ImageView mainImg;
    private  Guide guideobj;
    private boolean isLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashcreen);

        mainImg = (ImageView)findViewById(R.id.mainImg);

        checklanguage();

    }

    private void checklanguage(){

      /*  Resources res = getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale("DA".toLowerCase());
        res.updateConfiguration(conf, dm);


*/
        if(Locale.getDefault().getDisplayLanguage().equalsIgnoreCase("Danish")){

            SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("lang", "da");
            // editor.putString("userId ",edLoginPassword.getText().toString().trim());
            editor.commit();


        }else{

            SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("lang", "en");
            // editor.putString("userId ",edLoginPassword.getText().toString().trim());
            editor.commit();
        }


    }


    @Override
    protected void onResume() {
        super.onResume();


        mainImg.setBackgroundResource(R.drawable.splash);

        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(SplashScreenBAG.CONNECTIVITY_SERVICE);
        if(CheckInternetConnection(connectivityManager)){
            processfetchGUIDE();
        }
        else{
            CountDownTimer countDownTimer;
            countDownTimer = new MyCountDownTimer(1000, 1000,R.drawable.offlinesplash_en);
            countDownTimer.start();
        }
    }


private void processfetchGUIDE(){

        RequestJSON json = new RequestJSON();

        AppConstants obj= new AppConstants();

        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");


    String LINK = obj.setURL(AppConstantsBAG.PROJECT_BAG,lang,AppConstantsBAG.GUIDE);

        json.GET_METHOD(LINK);

        json.setResponseListener(new ResponseListener() {
            @Override
            public String response(String response) {

                guideobj = new GsonBuilder().create().fromJson(response, Guide.class);
                ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(SplashScreenBAG.this, "user_pref", 0);
                complexPreferences.putObject("guide", guideobj);
                complexPreferences.commit();

                checkLoginExist();
                return null;
            }

            @Override
            public String error(String error) {
                Log.e("excp err on welcome", error);
                return null;
            }
        });

    }

    private void checkLoginExist(){

        SharedPreferences preferences = getSharedPreferences("login", MODE_PRIVATE);
        isLogin = preferences.getBoolean("isUserLogin", false);


        if(isLogin){
            Intent i = new Intent(SplashScreenBAG.this,HomeScreenBAG.class);
            startActivity(i);
            finish();
        }

        else if(guideobj.guideForceLogin || !isLogin ){
            Intent i = new Intent(SplashScreenBAG.this,WelcomeScreenBAG.class);
            i.putExtra("classname","MenuScreen.class");
            startActivity(i);
            finish();
        }


    }

    public class MyCountDownTimer extends CountDownTimer {
        int backImage;
        public MyCountDownTimer(long startTime, long interval,int image) {
            super(startTime, interval);
            backImage=image;
        }
        @Override
        public void onFinish() {
            mainImg.setBackgroundResource(backImage);
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

    }
    //end of main class
}
