package com.project.bag.ui;

import android.content.Intent;
import android.os.Bundle;
import com.project.bag.R;
import com.project.client_library.ui.WelcomeScreen;
import android.view.View;


public class WelcomeScreenBAG extends WelcomeScreen {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {

            case R.id.btnCreateUser:
                Intent i = new Intent(WelcomeScreenBAG.this,CreateUserScreenBAG.class);
                startActivity(i);

                break;

            case R.id.btnLogin:
                Intent i2 = new Intent(WelcomeScreenBAG.this,LoginScreenBAG.class);
                startActivity(i2);

                break;
        }
    }
}
