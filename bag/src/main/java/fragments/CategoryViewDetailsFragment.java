package fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.project.bag.R;
import com.project.client_library.Cells.CategoryItemView;
import com.project.client_library.adapters.CategoryViewAdapter;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.interfaces.CategoryItemCell;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.CategoryDetailsStructureModel;
import com.project.client_library.widget.HUD;

import java.util.ArrayList;

import helpers.AppConstantsBAG;

public class CategoryViewDetailsFragment extends Fragment {
    private ListView listView;
    private Toolbar toolbar;
    private ArrayList<CategoryItemCell> categoryitems;
    private String title;
    CategoryDetailsStructureModel  catgobj;
    private HUD dialog;

    public static CategoryViewDetailsFragment newInstance(String param1, String param2) {
        CategoryViewDetailsFragment fragment = new CategoryViewDetailsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public CategoryViewDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertview = inflater.inflate(R.layout.fragment_categoryview, container, false);

        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_blue);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processBackbutton();
            }
        });

        Bundle getbundle = this.getArguments();
        title = getbundle.getString("frag-name");

        toolbar_Title.setText(title);


        listView= (ListView)convertview.findViewById(R.id.listview);
        proessfetchData();

        return convertview;
    }


    private void processBackbutton(){
        Fragment tab1 = (HomeFragment)getActivity().getSupportFragmentManager().findFragmentByTag("tab1");
        Fragment tab2 = (MyAreaFragment)getActivity().getSupportFragmentManager().findFragmentByTag("tab2");
        Fragment tab3 = (FavouriteFragment)getActivity().getSupportFragmentManager().findFragmentByTag("tab3");
        Fragment tab4 = (AboutUsFragment)getActivity().getSupportFragmentManager().findFragmentByTag("tab4");
        Fragment tab5 = (SignalFragment)getActivity().getSupportFragmentManager().findFragmentByTag("tab5");

        if(tab1 != null && tab1 instanceof HomeFragment) {
            //   Toast.makeText(HomeScreenBAG.this,"Already exists",Toast.LENGTH_SHORT).show();
            try {
                getActivity().onBackPressed();
            }catch (Exception e){
                HomeFragment hom = new HomeFragment();
                FragmentManager fragmentManager =  getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.realtabcontent, hom);
                fragmentTransaction.commit();


            }
        } else{
            getActivity().onBackPressed();
        }


    }


    private void proessfetchData(){

        dialog = new HUD(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);
        dialog.title("");
        dialog.show();
/*
        String type;

        if(isProductpage){
            type =AppConstantsBAG.CATEGORY_PRODUCTS;
        }else{
            type =AppConstantsBAG.CATEGORY_TOURS;
        }*/


        RequestJSON json = new RequestJSON();

        AppConstants obj= new AppConstants();
        SharedPreferences preferences = getActivity().getSharedPreferences("language", getActivity().MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsBAG.PROJECT_BAG,lang,AppConstantsBAG.CATEGORY_TOURS);

        json.GET_METHOD(LINK);

        json.setResponseListener(new ResponseListener() {
            @Override
            public String response(String response) {
                dialog.dismiss();
                Log.e("response-->", response);
                catgobj = new GsonBuilder().create().fromJson(response, CategoryDetailsStructureModel.class);
                initalize();

                //   ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(SplashScreenBAG.this, "user_pref", 0);
                //   complexPreferences.putObject("guide", guideobj);
                //   complexPreferences.commit();

                //checkLoginExist();
                return null;
            }

            @Override
            public String error(String error) {
                Log.e("excp err on welcome", error);
                return null;
            }
        });

    }

    public void initalize(){

        categoryitems = new ArrayList<CategoryItemCell>();
        // Is sub text availabel cell --- title -- subtitle --counter


        for(int i=0;i<catgobj.items.size();i++){
            categoryitems.add(new CategoryItemView(true,catgobj.items.get(i).thumbnail,catgobj.items.get(i).title,catgobj.items.get(i).subtitle,0));
        }

        CategoryViewAdapter adapter = new CategoryViewAdapter(getActivity(),categoryitems,getResources().getColor(R.color.beapBAGColor));
        listView.setAdapter(adapter);




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Bundle bundle = new Bundle();

                bundle.putString("toolbar-title",title);

               /* bundle.putString("frag-title",catgobj.items.get(position).title);
                bundle.putString("frag-subtitle",catgobj.items.get(position).subtitle);
               bundle.putInt("mainpos",position);
               */
                bundle.putInt("id",catgobj.items.get(position).id);

                ItemsDetailsFragment item = new ItemsDetailsFragment();
                FragmentManager fragmentManager1 = getActivity().getSupportFragmentManager();

                FragmentTransaction fragmentTransaction1 = fragmentManager1.beginTransaction();
                fragmentTransaction1.replace(R.id.realtabcontent, item);
                item.setArguments(bundle);

                fragmentTransaction1.addToBackStack(null);
                fragmentTransaction1.commit();
            }
        });


    /*    categoryitems.add(new CategoryItemView(false,"BMW 1-serie","",2));
        categoryitems.add(new CategoryItemView(false,"BMW 2-serie","",1));
        categoryitems.add(new CategoryItemView(false,"BMW 3-serie","",5));
        categoryitems.add(new CategoryItemView(false,"BMW 4-serie","",8));
        categoryitems.add(new CategoryItemView(false,"BMW 5-serie","",8));
        categoryitems.add(new CategoryItemView(false,"BMW 6-serie","",8));
        categoryitems.add(new CategoryItemView(false,"BMW 7-serie","",18));*/

       /* categoryitems.add(new CategoryItemView(true,"BMW 1-serievvvguvvvuvbjhbmnbnbmnbn","wwdwdwd",2));
        categoryitems.add(new CategoryItemView(true,"BMW 2-serie","wdwd dwd wd",1));
        categoryitems.add(new CategoryItemView(true,"BMW 3-serie","fewfw wef we",5));
        categoryitems.add(new CategoryItemView(true,"BMW 4-serie","wef wef w",8));
        categoryitems.add(new CategoryItemView(true,"BMW 5-serie","wef wef ",8));
        categoryitems.add(new CategoryItemView(true,"BMW 6-serie","e wfefjgcfgyxcfgxfgxfgxfthftytyftuftyftyftyftyfyftyftyftyfygxfgxfg",8));
        categoryitems.add(new CategoryItemView(true,"BMW 7-serie"," wfwef w",18));*/

    }

}
