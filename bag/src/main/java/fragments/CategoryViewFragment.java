package fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.project.bag.R;
import com.project.client_library.Cells.CategoryItemView;
import com.project.client_library.adapters.CategoryViewAdapter;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.CategoryItemCell;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.CategoryStructureModel;
import com.project.client_library.widget.HUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import helpers.AppConstantsBAG;

public class CategoryViewFragment extends Fragment {
    private ListView listView;
    private Toolbar toolbar;
    private ArrayList<CategoryItemCell> categoryitems;
    private String title;
    CategoryStructureModel  catgobj;
    private HUD dialog;
    private JSONObject jsonobj;

    public static CategoryViewFragment newInstance(String param1, String param2) {
        CategoryViewFragment fragment = new CategoryViewFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public CategoryViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertview = inflater.inflate(R.layout.fragment_categoryview, container, false);

        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_blue);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                processBackbutton();

            }
        });

        Bundle getbundle = this.getArguments();
        title = getbundle.getString("frag-name");
        toolbar_Title.setText(title);


        listView= (ListView)convertview.findViewById(R.id.listview);
        proessfetchData();






        return convertview;
    }

    private void processBackbutton(){
        Fragment tab1 = (HomeFragment)getActivity().getSupportFragmentManager().findFragmentByTag("tab1");
        Fragment tab2 = (MyAreaFragment)getActivity().getSupportFragmentManager().findFragmentByTag("tab2");
        Fragment tab3 = (FavouriteFragment)getActivity().getSupportFragmentManager().findFragmentByTag("tab3");
        Fragment tab4 = (AboutUsFragment)getActivity().getSupportFragmentManager().findFragmentByTag("tab4");
        Fragment tab5 = (SignalFragment)getActivity().getSupportFragmentManager().findFragmentByTag("tab5");

        if(tab1 != null && tab1 instanceof HomeFragment) {
            //   Toast.makeText(HomeScreenBAG.this,"Already exists",Toast.LENGTH_SHORT).show();
            try {
                getActivity().onBackPressed();
            }catch (Exception e){
                HomeFragment hom = new HomeFragment();
                FragmentManager fragmentManager =  getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.realtabcontent, hom);
                fragmentTransaction.commit();


            }
        } else{
            getActivity().onBackPressed();
        }


    }

    private void proessfetchData(){

        dialog = new HUD(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);
        dialog.title("");
        dialog.show();

      /*  String type;

        if(isProductpage){
            type =AppConstantsBAG.CATEGORY_PRODUCTS;
        }else{
            type =AppConstantsBAG.CATEGORY_TOURS;
        }
*/

        RequestJSON json = new RequestJSON();

        AppConstants obj= new AppConstants();

        SharedPreferences preferences = getActivity().getSharedPreferences("language", getActivity().MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsBAG.PROJECT_BAG,lang,AppConstantsBAG.CATEGORY_PRODUCTS);

        json.GET_METHOD(LINK);

        json.setResponseListener(new ResponseListener() {
            @Override
            public String response(String response) {
                dialog.dismiss();
                Log.e("response-->", response);

                try {
                    jsonobj = new JSONObject(response);
                } catch (Exception e) {

                }

                catgobj = new GsonBuilder().create().fromJson(response, CategoryStructureModel.class);

                initalize();

                //   ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(SplashScreenBAG.this, "user_pref", 0);
                //   complexPreferences.putObject("guide", guideobj);
                //   complexPreferences.commit();

                //checkLoginExist();
                return null;
            }

            @Override
            public String error(String error) {
                Log.e("excp err on welcome", error);
                return null;
            }
        });

    }

    public void initalize(){

        categoryitems = new ArrayList<CategoryItemCell>();


        // Is sub text availabel cell --- title -- subtitle --counter


        for(int i=0;i<catgobj.categories.size();i++){
            categoryitems.add(new CategoryItemView(false,catgobj.categories.get(i).thumbnail,catgobj.categories.get(i).name,"",catgobj.categories.get(i).count));
        }

        CategoryViewAdapter adapter = new CategoryViewAdapter(getActivity(),categoryitems,getResources().getColor(R.color.beapBAGColor));
        listView.setAdapter(adapter);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {

                    JSONArray jarray = new JSONArray(jsonobj.getJSONArray("categories").toString());
                    JSONObject newobj = jarray.getJSONObject(position);

                        if(newobj.has("items")){
                            processOpenItemCollectionView(position);
                        }else{
                            processOpenCategoryView(position);
                        }



                }catch(Exception e){
                    Log.e("exc",e.toString());
                }






            }
        });



    }




    private void processOpenCategoryView(int position){

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "user_pref", 0);
        complexPreferences.putObject("catg", catgobj);
        complexPreferences.commit();

        Bundle bundle = new Bundle();
        bundle.putString("frag-name",catgobj.categories.get(position).name);
        bundle.putInt("pos", position);
        SubCategoryViewFragment subcatg = new SubCategoryViewFragment();
        FragmentManager fragmentManager =  getActivity().getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.realtabcontent, subcatg);
        subcatg.setArguments(bundle);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void processOpenItemCollectionView(int position){

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "user_pref", 0);
        complexPreferences.putObject("catg", catgobj);
        complexPreferences.commit();

        Bundle bundle = new Bundle();
        bundle.putString("frag-name",catgobj.categories.get(position).name);
        bundle.putInt("pos", position);

        ItemscollectionviewFragment collectionview = new ItemscollectionviewFragment();
        FragmentManager fragmentManager =  getActivity().getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.realtabcontent, collectionview);
        collectionview.setArguments(bundle);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
