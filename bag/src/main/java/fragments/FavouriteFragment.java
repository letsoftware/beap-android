package fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.project.bag.R;
import com.project.client_library.Cells.CategoryItemView;
import com.project.client_library.adapters.CategoryViewAdapter;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.CategoryItemCell;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.FavouritesDetailsModel;
import com.project.client_library.model.UserModel;
import com.project.client_library.widget.HUD;

import java.util.ArrayList;

import helpers.AppConstantsBAG;

public class FavouriteFragment extends Fragment {
    private Toolbar toolbar;
    private HUD dialog;
    private ArrayList<CategoryItemCell> categoryitems;
    UserModel userObj;
    private ListView listView;
    LinearLayout linearEmpty,linearlist;
    FavouritesDetailsModel favmodel;

    public FavouriteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertview =  inflater.inflate(R.layout.fragment_favourite, container, false);
        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_Title.setText(getString(R.string.FAVOUROTE_TITLE));

        linearEmpty = (LinearLayout)convertview.findViewById(R.id.linearEmpty);
        linearlist = (LinearLayout)convertview.findViewById(R.id.linearlist);
        listView= (ListView)convertview.findViewById(R.id.listview);


        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "user_pref", 0);
        userObj = complexPreferences.getObject("current-user", UserModel.class);

        processfetchFavouritesItems();

        return convertview;
    }


    private void processfetchFavouritesItems(  ){

        dialog = new HUD(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);
        dialog.title("");
        dialog.show();

        RequestJSON json = new RequestJSON();

        AppConstants obj= new AppConstants();
        SharedPreferences preferences = getActivity().getSharedPreferences("language", getActivity().MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsBAG.PROJECT_BAG,lang, AppConstantsBAG.FAVOURITES+userObj.userId+"?detailed=1");

        json.GET_METHOD(LINK);

        json.setResponseListener(new ResponseListener() {
            @Override
            public String response(String response) {
                dialog.dismiss();

                Log.e("response fav-->", response);

                try {
                    favmodel = new GsonBuilder().create().fromJson(response, FavouritesDetailsModel.class);
                    processshowData(favmodel);

                } catch (Exception e) {
                    Log.e("exc", e.toString());
                }


                return null;
            }

            @Override
            public String error(String error) {
                dialog.dismiss();
                Log.e("excp err on welcome", error);
                return null;
            }
        });

    }


    private void processshowData(FavouritesDetailsModel favobj){

        if(favobj.count == 0){
                linearEmpty.setVisibility(View.VISIBLE);
                linearlist.setVisibility(View.GONE);
        }else{
                linearEmpty.setVisibility(View.GONE);
                linearlist.setVisibility(View.VISIBLE);
                initalize();

        }


    }

    public void initalize(){

        categoryitems = new ArrayList<CategoryItemCell>();
        // Is sub text availabel cell --- title -- subtitle --counter


        for(int i=0;i<favmodel.favItems.size();i++){
            categoryitems.add(new CategoryItemView(true,favmodel.favItems.get(i).thumbnail,favmodel.favItems.get(i).title,favmodel.favItems.get(i).subtitle,0));
        }

        CategoryViewAdapter adapter = new CategoryViewAdapter(getActivity(),categoryitems,getResources().getColor(R.color.beapBAGColor));
        listView.setAdapter(adapter);




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {




                String href = favmodel.favItems.get(position).href;

                //int  lastindex = hre
                String pid = href.substring(href.lastIndexOf("/")+1,href.length());


                Bundle bundle = new Bundle();

                bundle.putString("toolbar-title",getString(R.string.FAVOUROTE_TITLE));

                bundle.putInt("id",Integer.valueOf(pid));

                ItemsDetailsFragment item = new ItemsDetailsFragment();
                FragmentManager fragmentManager1 = getFragmentManager();

                FragmentTransaction fragmentTransaction1 = fragmentManager1.beginTransaction();
                fragmentTransaction1.replace(R.id.realtabcontent, item,"CATG");
                item.setArguments(bundle);

                fragmentTransaction1.addToBackStack(null);
                fragmentTransaction1.commit();
            }
        });


    /*    categoryitems.add(new CategoryItemView(false,"BMW 1-serie","",2));
        categoryitems.add(new CategoryItemView(false,"BMW 2-serie","",1));
        categoryitems.add(new CategoryItemView(false,"BMW 3-serie","",5));
        categoryitems.add(new CategoryItemView(false,"BMW 4-serie","",8));
        categoryitems.add(new CategoryItemView(false,"BMW 5-serie","",8));
        categoryitems.add(new CategoryItemView(false,"BMW 6-serie","",8));
        categoryitems.add(new CategoryItemView(false,"BMW 7-serie","",18));*/

       /* categoryitems.add(new CategoryItemView(true,"BMW 1-serievvvguvvvuvbjhbmnbnbmnbn","wwdwdwd",2));
        categoryitems.add(new CategoryItemView(true,"BMW 2-serie","wdwd dwd wd",1));
        categoryitems.add(new CategoryItemView(true,"BMW 3-serie","fewfw wef we",5));
        categoryitems.add(new CategoryItemView(true,"BMW 4-serie","wef wef w",8));
        categoryitems.add(new CategoryItemView(true,"BMW 5-serie","wef wef ",8));
        categoryitems.add(new CategoryItemView(true,"BMW 6-serie","e wfefjgcfgyxcfgxfgxfgxfthftytyftuftyftyftyftyfyftyftyftyfygxfgxfg",8));
        categoryitems.add(new CategoryItemView(true,"BMW 7-serie"," wfwef w",18));*/

    }

//end of main class
}
