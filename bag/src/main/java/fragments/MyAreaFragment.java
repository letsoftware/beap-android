package fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.project.bag.R;
import com.project.bag.ui.CreateUserScreenBAG;
import com.project.bag.ui.WelcomeScreenBAG;
import com.project.client_library.Cells.MyAreaView;
import com.project.client_library.adapters.MyAreaAdapter;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.MyAreaCell;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.NewsStructureModel;
import com.project.client_library.widget.HUD;

import java.util.ArrayList;

import helpers.AppConstantsBAG;

public class MyAreaFragment extends Fragment {

    private ListView listView_Area;
    private HUD dialog;
    private Toolbar toolbar;
    private ArrayList<MyAreaCell> myarea_items;
    TextView txtCounter;

    public MyAreaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        initalize();

        MyAreaAdapter adapter = new MyAreaAdapter(getActivity(),myarea_items,getResources().getColor(R.color.beapBAGColor));
        listView_Area.setAdapter(adapter);

        processfetchNEWS();


        listView_Area.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentManager fragmentManager;
                FragmentTransaction fragmentTransaction;
                switch (position) {

                    case 0:
                        Intent intentProfile = new Intent(getActivity(), CreateUserScreenBAG.class);
                        intentProfile.putExtra("isEditProfile", true);
                        startActivity(intentProfile);
                        break;
                    case 1:
                        SettingsFragment settingsFragment = new SettingsFragment();
                        fragmentManager= getFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.realtabcontent, settingsFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        break;

                    case 2:
                        Bundle bundle = new Bundle();
                        bundle.putString("frag-name", getString(R.string.NEWS_TITLE));

                        NewsFragment newsFragment = new NewsFragment();
                        fragmentManager = getFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.realtabcontent, newsFragment);
                        newsFragment.setArguments(bundle);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                        break;
                    case 4:
                        SharedPreferences preferences = getActivity().getSharedPreferences("login", getActivity().MODE_PRIVATE);
                        SharedPreferences.Editor ed  = preferences.edit();
                        ed.putBoolean("isUserLogin", false);
                        ed.commit();

                       Intent i = new Intent(getActivity(), WelcomeScreenBAG.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        getActivity().finish();



                        break;
                }
            }
        });

    }

    private void processfetchNEWS() {

        dialog = new HUD(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);
        dialog.title("");
        dialog.show();

        RequestJSON json = new RequestJSON();

        AppConstants obj= new AppConstants();
        SharedPreferences preferences = getActivity().getSharedPreferences("language", getActivity().MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsBAG.PROJECT_BAG,lang,AppConstantsBAG.NEWS);

        json.GET_METHOD(LINK);

        json.setResponseListener(new ResponseListener() {
            @Override
            public String response(String response) {
                dialog.dismiss();
                Log.e("response:", response + "");
                NewsStructureModel newsStructureModel = new GsonBuilder().create().fromJson(response, NewsStructureModel.class);
                ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "news_pref", 0);
                complexPreferences.putObject("news_details", newsStructureModel);
                complexPreferences.commit();
                for (int i = 0; i < listView_Area.getAdapter().getCount(); i++) {

                    if (myarea_items.get(i).isNewsCell()) {
                        View newsView;
                        newsView = listView_Area.getChildAt(i);
                        TextView txtCounter = (TextView) newsView.findViewById(R.id.txtCounter);
                        txtCounter.setText(newsStructureModel.count + "");
                    }
                }

                return null;
            }

            @Override
            public String error(String error) {
                Log.e("excp err on welcome", error);
                return null;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View convertview = inflater.inflate(R.layout.fragment_myarea, container, false);
        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);



        toolbar_Title.setText(getString(R.string.MY_AREA));
        listView_Area = (ListView)convertview.findViewById(R.id.listview);




        return convertview;
    }


    public void initalize(){

        myarea_items = new ArrayList<MyAreaCell>();

        // switch cell - blank cell -- normal cell-- title
        myarea_items.add(new MyAreaView(false,false,getString(R.string.YOUR_PROFILE_TITLE)));
        myarea_items.add(new MyAreaView(false,false,getString(R.string.SETTINGS_TITLE)));
        myarea_items.add(new MyAreaView(true,false,getString(R.string.NEWS_TITLE)));
        myarea_items.add(new MyAreaView(false,true,""));
        myarea_items.add(new MyAreaView(false,false,getString(R.string.LOGOUT_TITLE)));
    }

}
