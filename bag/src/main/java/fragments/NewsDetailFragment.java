package fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.project.bag.R;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.model.NewsStructureModel;

public class NewsDetailFragment extends Fragment {
    private Toolbar toolbar;
    private String title;
    private int position;
    private ImageView imgAboutUs;
    NewsStructureModel newsStructureModel;
    private TextView txtTitle,txtDate,txtDescription;
    public static NewsDetailFragment newInstance(String param1, String param2) {
        NewsDetailFragment fragment = new NewsDetailFragment();

        return fragment;
    }

    public NewsDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertview= inflater.inflate(R.layout.fragment_news_detail, container, false);
        imgAboutUs= (ImageView) convertview.findViewById(R.id.imgAboutUs);

        txtTitle= (TextView) convertview.findViewById(R.id.txtTitle);
        txtDate= (TextView) convertview.findViewById(R.id.txtDate);
        txtDescription= (TextView) convertview.findViewById(R.id.txtDescription);

        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_blue);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        Bundle getbundle = this.getArguments();
        title = getbundle.getString("frag-name");
        position= getbundle.getInt("pos");

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "news_pref", 0);
        newsStructureModel=complexPreferences.getObject("news_details", NewsStructureModel.class);

        txtTitle.setText(newsStructureModel.newsList.get(position).title);
        txtDate.setText(newsStructureModel.newsList.get(position).publication_date);

        txtDate.setTextColor(getResources().getColor(R.color.beapBAGColor));

        txtDescription.setText(newsStructureModel.newsList.get(position).content);

        Glide.with(getActivity()).load(newsStructureModel.newsList.get(position).thumbnail).thumbnail(0.1f).into(imgAboutUs);
        toolbar_Title.setText(title);
        return convertview;
    }



}
