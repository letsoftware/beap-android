package fragments;

import android.app.Activity;
import android.net.Uri;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.project.bag.R;
import com.project.client_library.Cells.CategoryItemView;
import com.project.client_library.adapters.CategoryViewAdapter;
import com.project.client_library.adapters.CategoryViewAdapterNEWS;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.CategoryItemCell;
import com.project.client_library.model.CategoryDetailsStructureModel;
import com.project.client_library.model.NewsStructureModel;
import com.project.client_library.widget.HUD;

import java.util.ArrayList;


public class NewsFragment extends Fragment {
    private ListView listView;
    private Toolbar toolbar;
    private ArrayList<CategoryItemCell> categoryitems;
    private String title;
    NewsStructureModel newsStructureModel;
    private HUD dialog;

    public static NewsFragment newInstance(String param1, String param2) {
        NewsFragment fragment = new NewsFragment();
        return fragment;
    }


    public NewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertview= inflater.inflate(R.layout.fragment_news, container, false);
        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_blue);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        Bundle getbundle = this.getArguments();
        title = getbundle.getString("frag-name");

        toolbar_Title.setText(title);


        listView= (ListView)convertview.findViewById(R.id.listview);
        initalize();
        return  convertview;
    }

    public void initalize(){

        categoryitems = new ArrayList<CategoryItemCell>();
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "news_pref", 0);
         newsStructureModel=complexPreferences.getObject("news_details", NewsStructureModel.class);


        for(int i=0;i<newsStructureModel.newsList.size();i++){
            categoryitems.add(new CategoryItemView(true,true,newsStructureModel.newsList.get(i).title,newsStructureModel.newsList.get(i).subtitle,newsStructureModel.count,newsStructureModel.newsList.get(i).publication_date));
        }

        CategoryViewAdapterNEWS adapter = new CategoryViewAdapterNEWS(getActivity(),categoryitems,getResources().getColor(R.color.beapBAGColor));

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                processOpenNewsView(position);
            }
        });

    }

    private void processOpenNewsView(int position) {


        Bundle bundle = new Bundle();
        bundle.putString("frag-name",newsStructureModel.newsList.get(position).title);
        bundle.putInt("pos", position);
        NewsDetailFragment subcatg = new NewsDetailFragment();
        FragmentManager fragmentManager = getFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.realtabcontent, subcatg);
        subcatg.setArguments(bundle);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
