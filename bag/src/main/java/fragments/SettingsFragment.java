package fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import com.project.bag.R;
import com.project.client_library.Cells.CellButton;
import com.project.client_library.Cells.SettingView;
import com.project.client_library.adapters.SettingsAdapter;
import com.project.client_library.interfaces.SettingCell;

import java.util.ArrayList;

public class SettingsFragment extends Fragment {
    private ListView listView_Settings;
    private Toolbar toolbar;
    private ArrayList<SettingCell> settingsitems;

    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View convertview = inflater.inflate(R.layout.fragment_settings, container, false);

        SharedPreferences preferences = getActivity().getSharedPreferences("lock", getActivity().MODE_PRIVATE);
        if(preferences.getBoolean("isLock",false)){
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }




        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_blue);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });


       /* toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyAreaFragment settingsFragment = new MyAreaFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.realtabcontent, settingsFragment);
                fragmentTransaction.commit();
            }
        });
*/
        toolbar_Title.setText(getString(R.string.SETTINGS_TITLE));


        listView_Settings= (ListView)convertview.findViewById(R.id.listviewSettings);
        initalize();



        SettingsAdapter adapter = new SettingsAdapter(getActivity(),settingsitems);
        listView_Settings.setAdapter(adapter);


        return convertview;
    }


    public void initalize(){

        settingsitems = new ArrayList<SettingCell>();
        String versionName="";
        try {
            versionName = "Version "+getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 1).versionName;
        }catch (Exception e){

        }
        // switch cell --- normal cell-- title
        settingsitems.add(new SettingView(true,false,getString(R.string.ENTERANCE_NOTIFICATION)));
        settingsitems.add(new SettingView(true,false,getString(R.string.AUTOMATIC_LOCK)));
        settingsitems.add(new SettingView(true,false,getString(R.string.VIBRATE_NOTIFICATIONS)));
        settingsitems.add(new SettingView(false,true,versionName));
    }

}
