package fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.project.bag.R;
import com.project.client_library.Cells.CategoryItemView;
import com.project.client_library.adapters.CategoryViewAdapter;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.CategoryItemCell;
import com.project.client_library.model.CategoryStructureModel;
import com.project.client_library.widget.HUD;

import java.util.ArrayList;

public class SubCategoryViewFragment extends Fragment {
    private ListView listView;
    private Toolbar toolbar;
    private ArrayList<CategoryItemCell> categoryitems;
    private String title;
    private int main_poss;
    CategoryStructureModel  catgobj;
    private HUD dialog;

    public static SubCategoryViewFragment newInstance(String param1, String param2) {
        SubCategoryViewFragment fragment = new SubCategoryViewFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public SubCategoryViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);




       /* if(getbundle.getString("frag-name").equalsIgnoreCase("product")){
            isProductpage=true;
        } else if(getbundle.getString("frag-name").equalsIgnoreCase("tour")){
            isProductpage=false;
        }*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertview = inflater.inflate(R.layout.fragment_categoryview, container, false);

        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_blue);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        Bundle getbundle = this.getArguments();
        title = getbundle.getString("frag-name");
        main_poss = getbundle.getInt("pos");

        toolbar_Title.setText(title);


        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "user_pref", 0);
        catgobj = complexPreferences.getObject("catg", CategoryStructureModel.class);



        listView= (ListView)convertview.findViewById(R.id.listview);
        initalize();






        return convertview;
    }



    public void initalize(){

        categoryitems = new ArrayList<CategoryItemCell>();
        // Is sub text availabel cell --- title -- subtitle --counter
        for(int i=0;i<catgobj.categories.get(main_poss).subcategory.size();i++){
            categoryitems.add(new CategoryItemView(false,catgobj.categories.get(main_poss).subcategory.get(i).thumbnail,catgobj.categories.get(main_poss).subcategory.get(i).name,"",catgobj.categories.get(main_poss).subcategory.get(i).count));
        }

        CategoryViewAdapter adapter = new CategoryViewAdapter(getActivity(),categoryitems,getResources().getColor(R.color.beapBAGColor));
        listView.setAdapter(adapter);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "user_pref", 0);
                complexPreferences.putObject("catg", catgobj);
                complexPreferences.commit();

                Bundle bundle = new Bundle();
                bundle.putString("frag-name",catgobj.categories.get(main_poss).subcategory.get(position).name);
                bundle.putInt("pos", main_poss);
                bundle.putInt("sub_pos", position);
                bundle.putBoolean("issubCatg", true);

                ItemscollectionviewFragment collectionview = new ItemscollectionviewFragment();
                FragmentManager fragmentManager =  getActivity().getSupportFragmentManager();

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.realtabcontent, collectionview);
                collectionview.setArguments(bundle);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });



    }

}
