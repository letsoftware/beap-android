package com.project.client_library.Cells;

import com.project.client_library.interfaces.CategoryItemCell;
import com.project.client_library.interfaces.MyAreaCell;

/**
 * Created by Android on 09-04-2015.
 */
public class CategoryItemView implements CategoryItemCell {

    public boolean isSubText;
    public String publication_date;
    public String imagUrl;
    public String Title;
    public String SubTitle;
    public int Counter;
    public boolean isHideImage;

    public CategoryItemView(boolean issubtext, String imgurl,String title, String subtitle, int counter){

        this.isSubText = issubtext;
        this.Title = title;
        this.SubTitle = subtitle;
        this.Counter = counter;
        this.imagUrl=imgurl;
    }

    public CategoryItemView(boolean issubtext,boolean isHideImage,String title, String subtitle, int counter){

        this.isSubText = issubtext;
        this.Title = title;
        this.SubTitle = subtitle;
        this.Counter = counter;
        this.isHideImage=isHideImage;
    }

    public CategoryItemView(boolean issubtext,boolean isHideImage,String title, String subtitle, int counter,String publishdate){

        this.isSubText = issubtext;
        this.Title = title;
        this.SubTitle = subtitle;
        this.Counter = counter;
        this.isHideImage=isHideImage;
        this.publication_date = publishdate;

    }



    @Override
    public boolean isSubText() {
        if(isSubText ==  true)
            return true;

        else
            return false;
    }
}
