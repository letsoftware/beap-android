package com.project.client_library.Cells;

import com.project.client_library.interfaces.CheckButton;

/**
 * Created by Android on 09-04-2015.
 */
public class CellButton implements CheckButton {

    boolean isRadio;
    boolean isSwitch;

    public int switchValue;
    public int radioValue;


    public CellButton(boolean forradio,boolean forswitch,int radioValue,int switchValue){
        this.isRadio = forradio;
        this.isSwitch = forswitch;
        this.switchValue=switchValue;
        this.radioValue=radioValue;
    }

    @Override
    public boolean isSwitchButton() {
        if(isSwitch ==  true)
        return true;

        else
            return false;
    }

    @Override
    public boolean isRadioButton() {
        if(isRadio ==  true)
            return true;

        else
            return false;
    }


}
