package com.project.client_library.Cells;

import com.project.client_library.interfaces.CheckButton;

/**
 * Created by Android on 09-04-2015.
 */
public class CellStandard implements CheckButton {

    @Override
    public boolean isSwitchButton() {
        return false;
    }

    @Override
    public boolean isRadioButton() {
        return false;
    }

    public CellStandard(boolean isTop, boolean isMiddle, boolean isBottom, int inputType, String hintValue, String etValue) {
        this.isTop = isTop;
        this.isMiddle = isMiddle;
        this.isBottom = isBottom;
        this.inputType = inputType;
        this.hintValue = hintValue;
        this.etValue=etValue;
    }

    public boolean isTop;

    public boolean isMiddle;

    public boolean isBottom;

    public int inputType;

    public String hintValue;

    public String etValue;

}
