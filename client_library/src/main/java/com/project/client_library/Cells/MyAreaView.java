package com.project.client_library.Cells;

import com.project.client_library.interfaces.MyAreaCell;
import com.project.client_library.interfaces.SettingCell;

/**
 * Created by Android on 09-04-2015.
 */
public class MyAreaView implements MyAreaCell {

    public boolean isBlank;
    public boolean isNews;
    public String Title;


    public MyAreaView(boolean fornews,boolean forblank,String title){
        this.isNews = fornews;
        this.isBlank = forblank;
        this.Title = title;
    }

    @Override
    public boolean isNewsCell() {
        if(isNews ==  true)
            return true;

        else
            return false;
    }

    @Override
    public boolean isBlankCell() {
        if(isBlank ==  true)
            return true;

        else
            return false;
    }
}
