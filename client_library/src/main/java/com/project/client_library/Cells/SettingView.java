package com.project.client_library.Cells;

import com.project.client_library.interfaces.CheckButton;
import com.project.client_library.interfaces.SettingCell;

/**
 * Created by Android on 09-04-2015.
 */
public class SettingView implements SettingCell {

    public boolean isText;
    public boolean isSwitch;
    public String Title;


    public SettingView(boolean forswitch,boolean fortext,String title){
        this.isSwitch = forswitch;
        this.isText = fortext;
        this.Title = title;
    }

    @Override
    public boolean isSwitchCell() {
        if(isSwitch ==  true)
            return true;

        else
            return false;
    }

    @Override
    public boolean isTextCell() {
        if(isText ==  true)
            return true;

        else
            return false;
    }
}
