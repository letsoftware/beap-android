package com.project.client_library.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.project.client_library.Cells.CategoryItemView;
import com.project.client_library.Cells.SettingView;
import com.project.client_library.R;
import com.project.client_library.interfaces.CategoryItemCell;
import com.project.client_library.interfaces.SettingCell;

import java.util.ArrayList;

public class CategoryViewAdapter extends BaseAdapter {

    int baseColor;
    LayoutInflater layoutInflator;
    private Context ctx;
    private ArrayList<CategoryItemCell> categoryitems;

    public CategoryViewAdapter(Context ctx, ArrayList<CategoryItemCell> items,int col) {
        this.categoryitems = items;
        this.ctx = ctx;
        this.baseColor = col;
    }

    @Override
    public int getCount() {
        return categoryitems.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryitems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final CategoryItemCell i = categoryitems.get(position);
        layoutInflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = convertView;
        view = layoutInflator.inflate(R.layout.category_item_view, parent, false);

        CategoryItemView catgview=(CategoryItemView)i;

        ImageView imgIcon = (ImageView)view.findViewById(R.id.imgIcon);
        TextView txtTitle = (TextView)view.findViewById(R.id.txt_Title);
        TextView txt_sub_Title = (TextView)view.findViewById(R.id.txt_sub_Title);
        TextView txtCounter = (TextView)view.findViewById(R.id.txtCounter);
        if(catgview.isHideImage){
            imgIcon.setVisibility(View.GONE);
        }else {
            imgIcon.setVisibility(View.VISIBLE);
            Glide.with(ctx).load(catgview.imagUrl).thumbnail(0.1f).into(imgIcon);
        }

        txtCounter.setTextColor(baseColor);

        if (i.isSubText()) {
            txtCounter.setVisibility(View.GONE);
            txt_sub_Title.setVisibility(View.VISIBLE);
            txtTitle.setText(catgview.Title);
            txt_sub_Title.setText(catgview.SubTitle);

        }
        else {
            txtTitle.setText(catgview.Title);
            txt_sub_Title.setVisibility(View.GONE);
            txtCounter.setVisibility(View.VISIBLE);
            txtCounter.setText(""+catgview.Counter);
        }

        return view;
    }
}
