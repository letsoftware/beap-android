package com.project.client_library.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.project.client_library.Cells.CellButton;
import com.project.client_library.Cells.CellStandard;

import com.project.client_library.interfaces.CheckButton;
import com.project.client_library.widget.SegmentedGroup;
import com.project.client_library.R;

import java.util.ArrayList;



public class CreateUserAdapter extends BaseAdapter {

    LayoutInflater layoutInflator;
    private Context ctx;
    private ArrayList<CheckButton> createScreenCellModelArrayList;
    int gender;

    public CreateUserAdapter(Context ctx, ArrayList<CheckButton> createScreenCellModelArrayList) {
        this.createScreenCellModelArrayList = createScreenCellModelArrayList;
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        if (createScreenCellModelArrayList == null) return 0;

        return createScreenCellModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        if (createScreenCellModelArrayList == null) return null;
        return createScreenCellModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final CheckButton i = createScreenCellModelArrayList.get(position);
        View v1 = convertView;
        View v2 = convertView;
        View topView = convertView;
        layoutInflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (i.isRadioButton()) {
            v1 = layoutInflator.inflate(R.layout.radio_item_view, parent, false);
            final CellButton cellButton=(CellButton)i;
            SegmentedGroup segmentGroup = (SegmentedGroup) v1.findViewById(R.id.segmentedGroup);
             RadioButton radioButtonMale = (RadioButton)segmentGroup.findViewById(R.id.rbMand);
             RadioButton radioButtonFeMale = (RadioButton)segmentGroup.findViewById(R.id.rbKvinde);
//            Toast.makeText(ctx,cellButton.radioValue+"",Toast.LENGTH_LONG).show();
            if(cellButton.radioValue==1){
                radioButtonMale.setChecked(true);
            } else {
                radioButtonFeMale.setChecked(true);
            }
            segmentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton radioButton = (RadioButton)group.findViewById(checkedId);

                    if(radioButton.getText().toString().equalsIgnoreCase(ctx.getString(R.string.male))){
                        cellButton.radioValue=1;
                    } else {
                        cellButton.radioValue=2;
                    }
                }
            });

            return v1;
        }  else if (i.isSwitchButton()) {
            v1 = layoutInflator.inflate(R.layout.switch_item_view, parent, false);
            final CellButton cellButton=(CellButton)i;

            final ToggleButton swNews = (ToggleButton) v1.findViewById(R.id.switchView);

                    if(cellButton.switchValue==0){
                        swNews.setChecked(false);
                    } else {
                        swNews.setChecked(true);
                    }
                    swNews.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                            if (isChecked) {
                                cellButton.switchValue = 1;
                                Log.e("switch value",cellButton.switchValue+"");

                            } else {
                                cellButton.switchValue = 0;
                                Log.e("switch value",cellButton.switchValue+"");

                            }
                        }
                    });

            return v1;
        }
        else {

            v2 = layoutInflator.inflate(R.layout.center_item_view, parent, false);
            final EditText etField= (EditText) v2.findViewById(R.id.etField);
            final CellStandard cellStandard=(CellStandard)i;
            etField.setHint(cellStandard.hintValue);
            etField.setInputType(cellStandard.inputType);
            etField.setText(cellStandard.etValue);
            if(cellStandard.isTop){
                etField.setBackgroundResource(R.drawable.celltop);
            } else if(cellStandard.isBottom){
                etField.setBackgroundResource(R.drawable.cellbottom);
            } else {
                etField.setBackgroundResource(R.drawable.cellmiddlle);
            }




            etField.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    cellStandard.etValue=etField.getText().toString().trim();
                    if(s.length()!=0){

                        Drawable img = ctx.getResources().getDrawable(android.R.drawable.presence_offline);

                        etField.setCompoundDrawablesWithIntrinsicBounds( null, null, img, null);

                        etField.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                final int DRAWABLE_LEFT = 0;
                                final int DRAWABLE_TOP = 1;
                                final int DRAWABLE_RIGHT = 2;
                                final int DRAWABLE_BOTTOM = 3;

                                if(event.getAction() == MotionEvent.ACTION_UP) {
                                    if(event.getRawX() >= (etField.getRight() - etField.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                        etField.setText("");
                                        return true;
                                    }
                                }
                                else{

                                }
                                return false;
                            }
                        });

                    }
                    else{
                        etField.setCompoundDrawablesWithIntrinsicBounds( null, null, null, null);
                        etField.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                return false;
                            }
                        });
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            return v2;


        }
    }
}
