package com.project.client_library.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.project.client_library.R;
import com.project.client_library.model.Guide;
import com.project.client_library.model.HeadlinesStructureModel;

/**
 * Created by Android on 01-04-2015.
 */
public class ImageViewPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        private HeadlinesStructureModel  imageitems;

        public ImageViewPagerAdapter(Context context,HeadlinesStructureModel images) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imageitems = images;
    }

        @Override
        public int getCount() {
        return imageitems.headlines.size();
    }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_viewpager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);


        Glide.with(mContext).load(imageitems.headlines.get(position).thumbnail).thumbnail(0.1f).into(imageView);
        container.addView(itemView);

        return itemView;
    }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

        @Override
        public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

}
