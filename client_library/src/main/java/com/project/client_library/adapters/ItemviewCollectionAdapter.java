package com.project.client_library.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.project.client_library.R;
import com.project.client_library.interfaces.ButtonClickListner;
import com.project.client_library.model.CategoryStructureModel;

/**
 * Created by Android on 01-04-2015.
 */

public class ItemviewCollectionAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    private CategoryStructureModel catgobj;
    private int Main_pos;
    private int Sub_pos;
    private boolean isSubCatg;
    ButtonClickListner clickobj;

    public ItemviewCollectionAdapter(Context context,CategoryStructureModel obj,int pos,int sub_pos,boolean issubcatg) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        catgobj = obj;
        Main_pos = pos;
        Sub_pos = sub_pos;
        isSubCatg = issubcatg;
    }

    public void setButtonlistener(ButtonClickListner listner){
        this.clickobj = listner;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_collection_view, container, false);

        ImageView itemImage = (ImageView) itemView.findViewById(R.id.itemImage);
        TextView txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        TextView txtSubTitle = (TextView) itemView.findViewById(R.id.txtSubTitle);
        TextView txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);

        if(isSubCatg) {
            txtTitle.setText(catgobj.categories.get(Main_pos).subcategory.get(Sub_pos).items.get(position).title);
            txtSubTitle.setText(catgobj.categories.get(Main_pos).subcategory.get(Sub_pos).items.get(position).subtitle);
            txtDesc.setText(catgobj.categories.get(Main_pos).subcategory.get(Sub_pos).items.get(position).content);

            Glide.with(mContext).load(catgobj.categories.get(Main_pos).subcategory.get(Sub_pos).items.get(position).main_image).thumbnail(0.1f).into(itemImage);

        }else {
            txtTitle.setText(catgobj.categories.get(Main_pos).items.get(position).title);
            txtSubTitle.setText(catgobj.categories.get(Main_pos).items.get(position).subtitle);
            txtDesc.setText(catgobj.categories.get(Main_pos).items.get(position).content);

            Glide.with(mContext).load(catgobj.categories.get(Main_pos).items.get(position).main_image).thumbnail(0.1f).into(itemImage);

        }




        container.addView(itemView);




        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickobj.ButtonClick(position);


            }
        });

        return itemView;
    }




    @Override
    public int getCount() {

        if(isSubCatg) {
            return catgobj.categories.get(Main_pos).subcategory.get(Sub_pos).items.size();
        }else {
             return catgobj.categories.get(Main_pos).items.size();
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }




}
