package com.project.client_library.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.project.client_library.Cells.MyAreaView;
import com.project.client_library.Cells.SettingView;
import com.project.client_library.R;
import com.project.client_library.interfaces.MyAreaCell;
import com.project.client_library.interfaces.SettingCell;

import java.util.ArrayList;

public class MyAreaAdapter extends BaseAdapter {


    LayoutInflater layoutInflator;
    private Context ctx;
    int baseColor;
    private ArrayList<MyAreaCell> myarea_items;
    private int[] Images = {R.drawable.yourprofile,R.drawable.indstillinger,R.drawable.nyheder,R.drawable.yourprofile};
    private int[] Images2 = {R.drawable.yourprofilered,R.drawable.indstillingerred,R.drawable.nyhederred,R.drawable.yourprofilered};



    public MyAreaAdapter(Context ctx, ArrayList<MyAreaCell> items,int col) {
        this.myarea_items = items;
        this.ctx = ctx;
        this.baseColor = col;

    }

    @Override
    public int getCount() {
        return myarea_items.size();
    }

    @Override
    public Object getItem(int position) {
        return myarea_items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final MyAreaCell i = myarea_items.get(position);
        layoutInflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = convertView;
        View blank_view = convertView;
        view = layoutInflator.inflate(R.layout.myarea_item_view, parent, false);


        MyAreaView myarea=(MyAreaView)i;


        ImageView imgIcon = (ImageView)view.findViewById(R.id.imgIcon);
        TextView txtTitle = (TextView)view.findViewById(R.id.txt_Title);
        TextView txtCounter = (TextView)view.findViewById(R.id.txtCounter);


        int DACCol = Color.parseColor("#FF2B0E");
        if(baseColor == DACCol){
            if(position==3){

            }else if (position==4){
                imgIcon.setBackgroundResource(Images2[position-1]);
            } else{
                imgIcon.setBackgroundResource(Images2[position]);
            }
        }else{
            if(position==3){

            }else if (position==4){
                imgIcon.setBackgroundResource(Images[position-1]);
            } else{
                imgIcon.setBackgroundResource(Images[position]);
            }
        }

        txtTitle.setText(myarea.Title);

        if (i.isNewsCell()) {
            txtCounter.setVisibility(View.VISIBLE);
            txtTitle.setText(myarea.Title);
            txtTitle.setTextColor(Color.parseColor("#4C4E4E"));
            txtCounter.setTextColor(baseColor);

            return view;
         }
        else if(i.isBlankCell()){
            blank_view = layoutInflator.inflate(R.layout.blank_item_view, parent, false);
            return blank_view;
        }
        else {
            txtTitle.setTextColor(Color.parseColor("#4C4E4E"));
            txtCounter.setVisibility(View.GONE);
            txtTitle.setText(myarea.Title);

            return view;
        }

    }
}
