package com.project.client_library.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.project.client_library.R;
import com.project.client_library.model.HeadlinesStructureModel;
import com.project.client_library.model.ProductModel;

/**
 * Created by Android on 01-04-2015.
 */
public class ProductImageViewPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        private ProductModel  imageitems;

        public ProductImageViewPagerAdapter(Context context, ProductModel images) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imageitems = images;
    }

        @Override
        public int getCount() {
        return imageitems.images.size();
    }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_viewpager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);


        Glide.with(mContext).load(imageitems.images.get(position)).thumbnail(0.1f).into(imageView);
        container.addView(itemView);

        return itemView;
    }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

        @Override
        public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

}
