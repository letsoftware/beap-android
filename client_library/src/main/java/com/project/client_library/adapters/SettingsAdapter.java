package com.project.client_library.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.project.client_library.Cells.CellStandard;
import com.project.client_library.Cells.SettingView;
import com.project.client_library.R;
import com.project.client_library.interfaces.CheckButton;
import com.project.client_library.interfaces.SettingCell;

import java.util.ArrayList;

public class SettingsAdapter extends BaseAdapter {


    LayoutInflater layoutInflator;
    private Context ctx;
    private ArrayList<SettingCell> settingsitems;

    public SettingsAdapter(Context ctx, ArrayList<SettingCell> items) {
        this.settingsitems = items;
        this.ctx = ctx;

    }

    @Override
    public int getCount() {
        return settingsitems.size();
    }

    @Override
    public Object getItem(int position) {
        return settingsitems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final SettingCell i = settingsitems.get(position);
        layoutInflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = convertView;
        view = layoutInflator.inflate(R.layout.settings_item_view, parent, false);


        SettingView settingview=(SettingView)i;


        TextView txtTitle = (TextView)view.findViewById(R.id.txt_Title);
        ToggleButton swView = (ToggleButton)view.findViewById(R.id.switchView);






        swView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(position ==1){

                    SharedPreferences preferences = ctx.getSharedPreferences("lock", ctx.MODE_PRIVATE);
                    SharedPreferences.Editor ed = preferences.edit();

                    if(isChecked) {
                           ed.putBoolean("isLock", true);
                           ed.commit();
                    }else{
                        ed.putBoolean("isLock", false);
                        ed.commit();
                    }


                }

            }
        });


        if(position==1){
            SharedPreferences preferences = ctx.getSharedPreferences("lock", ctx.MODE_PRIVATE);
            if(preferences.getBoolean("isLock",false)){
                swView.setChecked(true);
            }else{
                swView.setChecked(false);
            }


        }



        if(position == 0 || position == 2){
            swView.setChecked(true);
        }


        if (i.isTextCell()) {
            swView.setVisibility(View.GONE);
            txtTitle.setText(settingview.Title);
            txtTitle.setTextColor(Color.parseColor("#D6D7DC"));
            txtTitle.setPadding(32,16,0,16);
        }
        else {
            txtTitle.setTextColor(Color.parseColor("#4C4E4E"));
            swView.setVisibility(View.VISIBLE);
            txtTitle.setText(settingview.Title);
        }

        return view;
    }
}
