package com.project.client_library.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.project.client_library.R;
import com.project.client_library.model.Guide;

/**
 * Created by Android on 01-04-2015.
 */
public class WelcomeViewPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        private Guide guideobj;

        public WelcomeViewPagerAdapter(Context context,Guide obj ) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        guideobj = obj;
    }

        @Override
        public int getCount() {
        return guideobj.marrPages.size();
    }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.welcome_viewpager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);

        TextView txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        TextView txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);

        txtTitle.setText(guideobj.marrPages.get(position).title);
        txtDesc.setText(guideobj.marrPages.get(position).description);
        Glide.with(mContext).load(guideobj.marrPages.get(position).image).thumbnail(0.1f).into(imageView);
        container.addView(itemView);

        return itemView;
    }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

        @Override
        public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

}
