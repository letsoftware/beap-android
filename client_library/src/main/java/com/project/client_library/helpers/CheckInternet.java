package com.project.client_library.helpers;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Android on 01-04-2015.
 */
public class CheckInternet {


    public static boolean CheckInternetConnection(ConnectivityManager connectivityManager) {

        if (connectivityManager == null) {
            return false;
        }

        NetworkInfo niMobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo niWifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if ((niMobile != null && niMobile.getState() == NetworkInfo.State.CONNECTED) ||
                (niWifi != null && niWifi.getState() == NetworkInfo.State.CONNECTED)) {
            return true;
        }
        else {
            return false;
        }
    }
}
