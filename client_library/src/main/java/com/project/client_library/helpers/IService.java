package com.project.client_library.helpers;

public interface IService {

	public void response(String response);
	public void error(String error);

}
