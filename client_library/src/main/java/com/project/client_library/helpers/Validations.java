package com.project.client_library.helpers;

import android.util.Patterns;
import android.widget.EditText;

import java.util.regex.Pattern;

/**
 * Created by Android on 10-04-2015.
 */
public class Validations {


    public static boolean isEmptyField(String param1) {

        boolean isEmpty = false;
        if (param1 == null || param1.toString().equalsIgnoreCase("")) {
            isEmpty = true;
        }
        return isEmpty;
    }

    public static boolean isPasswordMatch(String param1, String param2) {
        boolean isMatch = false;
        if (param1.toString().equals(param2.toString())) {
            isMatch = true;
        }
        return isMatch;
    }

    public static boolean isEmailMatch(String param1, String param2) {
        boolean isMatch = false;
        if (param1.toString().equals(param2.toString())) {
            isMatch = true;
        }
        return isMatch;
    }

    public static boolean isEmailPattern(String param1) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(param1.toString()).matches();
    }

}
