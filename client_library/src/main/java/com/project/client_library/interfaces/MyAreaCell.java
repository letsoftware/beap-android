package com.project.client_library.interfaces;

/**
 * Created by Android on 09-04-2015.
 */
public interface MyAreaCell {

    public boolean isNewsCell();

    public boolean isBlankCell();

}
