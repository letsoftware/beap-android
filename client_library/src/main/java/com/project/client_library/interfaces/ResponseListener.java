package com.project.client_library.interfaces;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by Android on 03-04-2015.
 */
public interface ResponseListener {

    public String response(String response);
    public String error(String error);



}
