package com.project.client_library.interfaces;

/**
 * Created by Android on 09-04-2015.
 */
public interface SettingCell {

    public boolean isSwitchCell();

    public boolean isTextCell();

}
