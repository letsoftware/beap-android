package com.project.client_library.jsonParsing;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.project.client_library.helpers.CallWebService;
import com.project.client_library.helpers.MyApplication;
import com.project.client_library.helpers.POSTAPI;
import com.project.client_library.interfaces.POSTResponseListener;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.widget.HUD;

import org.json.JSONObject;

import java.io.Reader;

/**
 * Created by Android on 03-04-2015.
 */
public class RequestJSON {

    private HUD dialog;
    private String responseMsg;
    private ResponseListener responseListener;
    private POSTResponseListener postResponseListener;

    public void setResponseListener(ResponseListener listener){
        this.responseListener = listener;
    }

    public void setPostResponseListener(POSTResponseListener listener){
        this.postResponseListener = listener;
    }

    public void GET_METHOD(String link) {

        Log.e("JSON", "GET link: " + link);

        try {
            new CallWebService(link, CallWebService.TYPE_JSONOBJECT) {

                @Override
                public void response(String response) {
                    Log.e("JSON", "JSON response: " + response);
                    responseListener.response(response);
                }

                @Override
                public void error(String error) {
                    Log.e("JSON", "JSON error response: " + error);
                    responseListener.error(error);
                }
            }.start();

        } catch (Exception e) {
            Log.e("JSON", "JSON response exception: " + e.toString());
            e.printStackTrace();
        }
    }

    public void POST(final Context ctx, final String link, final String jsonstring) {

        Log.e("JSON", "POST link: " + link + " json: " + jsonstring);

        new AsyncTask<Void,Void,Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new HUD(ctx, android.R.style.Theme_Translucent_NoTitleBar);
                dialog.title("");
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                try{
                    Reader readerForNone = POSTAPI.callWebservicePost(link, jsonstring);
                    StringBuffer response = new StringBuffer();
                    int i = 0;
                    do {
                        i = readerForNone.read();
                        char character = (char) i;
                        response.append(character);

                    } while (i != -1);
                    readerForNone.close();

                    response.setLength(response.length() - 1);
                    responseMsg = response.toString();

                    Log.e("JSON", "JSON response: " + responseMsg);

                } catch (Exception e){
                    Log.e("JSON", "JSON response exception: " + e.toString());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                postResponseListener.onPost(responseMsg);
                dialog.dismiss();
            }
        }.execute();

    }



    public void POST_METHOD(String link, JSONObject jsonobj){

        Log.e("JSON", "POST link: " + link + " json: " + jsonobj.toString());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, link, jsonobj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
               Log.e("response JSON",""+ response);
               responseListener.response(response.toString());

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON", "JSON error response: " + error);
                responseListener.error(error.toString());
            }
        });

        MyApplication.getInstance().addToRequestQueue(req);
    }
}
