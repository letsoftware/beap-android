package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Android on 15-04-2015.
 */
public class AboutUs {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("subtitle")
    public String subtitle;

    @SerializedName("content")
    public String content;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("publication_date")
    public String publication_date;

    @SerializedName("href")
    public String href;

}
