package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class CategoryStructureModel {

    @SerializedName("count")
    public int count;

    @SerializedName("href")
    public String href;

    @SerializedName("categories")
    public ArrayList<CategoryModel> categories;
}
