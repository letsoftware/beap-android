package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class FavouritesDetailsModel {

    @SerializedName("favourites")
    public ArrayList<FavouritesItemModel> favItems;

    @SerializedName("count")
    public int count;

    @SerializedName("href")
    public String href;


}
