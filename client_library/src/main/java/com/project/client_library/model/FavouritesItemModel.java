package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class FavouritesItemModel {


    @SerializedName("title")
    public String title;

    @SerializedName("subtitle")
    public String subtitle;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("href")
    public String href;


}
