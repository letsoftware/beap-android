package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class FavouritesModel {

    @SerializedName("favourites")
    public ArrayList<String> favItems;

    @SerializedName("count")
    public int count;

    @SerializedName("href")
    public String href;


}
