package com.project.client_library.model;

import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;
/**
 * Created by dhruvil on 30-03-2015.
 */
public class Guide {

    @SerializedName("id")
    public int guideId;

    @SerializedName("forceLogin")
    public boolean guideForceLogin;

    @SerializedName("background")
    public String guideBackground;

    @SerializedName("href")
    public String href;

    @SerializedName("pages")
    public ArrayList<Pages>marrPages;


}
