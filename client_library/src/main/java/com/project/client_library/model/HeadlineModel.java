package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class HeadlineModel {


    @SerializedName("id")
    public int id;

    @SerializedName("highlight_image")
    public String highlight_image;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("type")
    public String type;

    @SerializedName("href")
    public String href;

   /* public enum HeadLineTypeEnum{
        ContentTypeUnknown,
        ContentTypeCategory,
        ContentTypeNews,
        ContentTypeTour,
        ContentTypeProduct,
        ContentTypeFindvej,
        ContentTypeWifi
    }

    public int headlineId;

    public String headlineThumbnail;

    public String headlineHighlightImage;

    public String headlineType;

    public String headlineHref;


    public HeadLineTypeEnum getHeadlineType(){

        if(headlineType == null || headlineType.equalsIgnoreCase("")){
            return HeadLineTypeEnum.ContentTypeUnknown;
        }else if(headlineType.equalsIgnoreCase("category")){
            return HeadLineTypeEnum.ContentTypeCategory;
        }else if(headlineType.equalsIgnoreCase("news")){
            return HeadLineTypeEnum.ContentTypeNews;
        }else if(headlineType.equalsIgnoreCase("tour")){
            return HeadLineTypeEnum.ContentTypeTour;
        }else if(headlineType.equalsIgnoreCase("product")){
            return HeadLineTypeEnum.ContentTypeProduct;
        }else if(headlineType.equalsIgnoreCase("findvej")){
            return HeadLineTypeEnum.ContentTypeFindvej;
        }else if(headlineType.equalsIgnoreCase("wifi")){
            return HeadLineTypeEnum.ContentTypeWifi;
        }
        return HeadLineTypeEnum.ContentTypeUnknown;
    }

*/

}
