package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class HeadlinesStructureModel {

    @SerializedName("count")
    public int count;

    @SerializedName("href")
    public String href;

    @SerializedName("headlines")
    public ArrayList<HeadlineModel> headlines;
}
