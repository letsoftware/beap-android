package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class ItemModel {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("subtitle")
    public String subtitle;

    @SerializedName("content")
    public String content;

    @SerializedName("href")
    public String href;

    @SerializedName("main_image")
    public String main_image;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("publication_date")
    public String publication_date;


}
