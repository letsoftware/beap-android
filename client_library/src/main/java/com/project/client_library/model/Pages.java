package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class Pages {



    @SerializedName("image")
    public String image;

    @SerializedName("description")
    public String description;

    @SerializedName("title")
    public String title;


}
