package com.project.client_library.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class Poll {

    public int pollId;

    public String pollAfterText;

    public String pollDoneButtonText;

    public String pollNotificationText;

    public String pollParticipationText;

    public int pollNotificationAfterMinutes;

    public String pollTitle;

    public Date pollStartDate;

    public Date pollEndDate;

    public Date pollTimeToShow;

    public ArrayList marrQuestions;

    public String pollWelcomeImage;

    public String pollStartButtonText;

    public String pollWelcomeText;

    public boolean pollUseNotificationTime;

    //todo getnotification time method, etc..


}
