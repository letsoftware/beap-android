package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class ProductModel {

    @SerializedName("id")
    public int id;

    @SerializedName("content")
    public String content;

    @SerializedName("title")
    public String title;

    @SerializedName("subtitle")
    public String subtitle;

    @SerializedName("main_image")
    public String main_image;

    @SerializedName("images")
    public ArrayList<String> images;


    @SerializedName("files")
    public ArrayList<String> files;

    @SerializedName("videos")
    public ArrayList<String> videos;

    @SerializedName("geoRequirements")
    public int geoRequirements;

    @SerializedName("URL")
    public String URL;

    @SerializedName("href")
    public String href;

    @SerializedName("openURL")
    public int openURL;







//
//    public int type;
//
//    public int categoryId;
//
//    public String title;
//
//    public String image;
//
//    public String href;
//
//    public int count;
//
//    public ArrayList marrCategories;
//
//    public ArrayList marrItems;


}
