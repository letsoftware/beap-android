package com.project.client_library.model;

import java.util.ArrayList;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class Question {

    public enum QuestionType{
        QuestionTypeTextfield,
        QuestionTypeSingle,
        QuestionTypeMultiple,
    }

    public int questionId;

    public String questionTitle;

    public QuestionType questionType;

    public ArrayList marrPosibleAnswers;

    public ArrayList userAnswers;


}
