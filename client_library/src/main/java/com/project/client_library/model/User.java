package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class User {

    @SerializedName("uid")
    public int uid;

    @SerializedName("mail")
    public String userMail;

    @SerializedName("name")
    public String name;

    @SerializedName("gender")
    public int userGender;

    @SerializedName("birthday")
    public String userBirthday;

    @SerializedName("newsletter")
    public int userNewsletter;


    @SerializedName("car_registration_number")
    public String userCar_registration_number;

    @SerializedName("car_brand")
    public String userCar_brand;

    @SerializedName("is_facebook")
    public int userIs_facebook;

    @SerializedName("fb_uid")
    public String facebookID;


    public String userEncryptedPassword;

}
