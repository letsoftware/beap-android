package com.project.client_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dhruvil on 30-03-2015.
 */
public class UserModel {

    @SerializedName("uid")
    public int userId;

    @SerializedName("href")
    public String href;

    @SerializedName("user")
    public User User;

}
