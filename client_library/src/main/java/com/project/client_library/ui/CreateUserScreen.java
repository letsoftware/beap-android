package com.project.client_library.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ListView;

import com.project.client_library.adapters.CreateUserAdapter;
import com.project.client_library.interfaces.ButtonClickListner;
import com.project.client_library.interfaces.CheckButton;
import com.project.client_library.model.User;

import java.util.ArrayList;


public abstract class CreateUserScreen extends ActionBarActivity   {

    public static final int textType= InputType.TYPE_CLASS_TEXT;
    public static final int emailType=InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;
    public static final int passwordType=InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD;

    private CreateUserAdapter createUserAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
    }

    protected abstract int getLayoutResourceId();

    protected ViewGroup setAdapter(final Context ctx, final ArrayList<CheckButton> list, final ListView listView) {

        Log.e("CreateUserScreen", "setAdapter called");

        createUserAdapter = new CreateUserAdapter(ctx, list);
        ViewGroup footer = (ViewGroup) getLayoutInflater().inflate(com.project.client_library.R.layout.footer_item_view, listView, false);
        listView.addFooterView(footer);
        listView.setAdapter(createUserAdapter);

        return footer;
    }

    protected void updateAdapter() {

        Log.e("CreateUserScreen", "updateAdapter called");

        createUserAdapter.notifyDataSetChanged();
    }
}
