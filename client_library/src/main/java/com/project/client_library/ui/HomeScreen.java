package com.project.client_library.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;


public abstract class HomeScreen extends ActionBarActivity {
    private FragmentTabHost mTabHost;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());

    }


    protected abstract int getLayoutResourceId();


    @Override
    protected void onResume() {
        super.onResume();



    }



    //end of main class
}
