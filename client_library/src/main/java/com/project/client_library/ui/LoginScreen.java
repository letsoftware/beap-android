package com.project.client_library.ui;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;


public abstract class LoginScreen extends ActionBarActivity  {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());

    }

    protected abstract int getLayoutResourceId();


    //end of main class
}
