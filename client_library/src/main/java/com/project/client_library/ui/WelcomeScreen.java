package com.project.client_library.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.project.client_library.R;
import com.project.client_library.adapters.WelcomeViewPagerAdapter;
import com.project.client_library.circle_indicator.CirclePageIndicator;
import com.project.client_library.circle_indicator.PageIndicator;
import com.project.client_library.helpers.ComplexPreferences;

import com.project.client_library.model.Guide;


public class  WelcomeScreen extends ActionBarActivity implements View.OnClickListener {

    protected Guide guideObject;
    protected ImageView bgImg;
    protected ViewPager view;
    protected Button btnLogin, btnCreateUser;
    protected PageIndicator mIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResourceId());

        bgImg = (ImageView) findViewById(R.id.bgImg);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnCreateUser = (Button) findViewById(R.id.btnCreateUser);
        view = (ViewPager) findViewById(R.id.pager);
        mIndicator = (CirclePageIndicator)findViewById(R.id.guideIndicator);

        btnLogin.setOnClickListener(this);
        btnCreateUser.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setContent(getBaseContext(), bgImg);
        WelcomeViewPagerAdapter adapter = new WelcomeViewPagerAdapter(this, guideObject);
        view.setAdapter(adapter);
        mIndicator.setViewPager(view);
    }

    protected int getLayoutResourceId() {
        return R.layout.activity_welcomescreen;
    }

    public void setContent(Context ctx,ImageView img) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_pref", 0);
        guideObject = complexPreferences.getObject("guide", Guide.class);
        Glide.with(this).load(guideObject.guideBackground).thumbnail(0.1f).into(img);
    }

    public void onClick(View v) {
        // to be implemented in children classes
    }
}
