package com.project.client_library.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by Android on 07-04-2015.
 */
public class AlertBox {



    public static void showBox(final Context ctx,final String Title,String Msg){

    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
    alertDialog.setTitle(Title);

    // Setting Dialog Message
    alertDialog.setMessage(Msg);

    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog,int which) {

        }
    });

   /* // Setting Negative "NO" Button
    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            // Write your code here to invoke NO event

        }
    });
*/
    // Showing Alert Message
    alertDialog.show();
    }
}
