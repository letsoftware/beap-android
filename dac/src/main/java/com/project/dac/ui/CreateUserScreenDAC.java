package com.project.dac.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.project.client_library.helpers.AeSimpleSHA1;
import com.project.dac.R;
import com.project.client_library.Cells.CellButton;
import com.project.client_library.Cells.CellStandard;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.helpers.Validations;
import com.project.client_library.interfaces.CheckButton;
import com.project.client_library.interfaces.POSTResponseListener;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.User;
import com.project.client_library.model.UserModel;
import com.project.client_library.ui.CreateUserScreen;
import com.project.client_library.widget.AlertBox;
import com.project.client_library.widget.HUD;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;

import helpers.AppConstantsDAC;

import static com.project.client_library.helpers.CheckInternet.CheckInternetConnection;


public class CreateUserScreenDAC extends CreateUserScreen {

    private Toolbar toolbar;
    private TextView toolbarTitleTextView;
    private ViewGroup footer;
    private Button footerButton;
    private User userObject;
    private JSONObject userJSONObject;
    private HUD dialog;
    private ListView listViewCreateUser;
    private ArrayList<CheckButton> createScreenCellModelArrayList;
    private boolean isProfileEditable;
    private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("CreateUserScreenDAC", "onCreate called");

        //check profile is editable or not
        isProfileEditable = getIntent().getBooleanExtra("isEditProfile", false);

        listViewCreateUser = (ListView) findViewById(R.id.listviewCreateUser);

        createScreenCellModelArrayList = new ArrayList<>();

        footer = setAdapter(CreateUserScreenDAC.this, createScreenCellModelArrayList, listViewCreateUser);
        footerButton = (Button) footer.findViewById(R.id.btnCreateUser);

        setToolbar();

        userObject = new User();

        if(isProfileEditable) {

            toolbarTitleTextView.setText("Edit User");
            footerButton.setText("Save");

            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenDAC.this, "user_pref", 0);
            userModel = complexPreferences.getObject("current-user", UserModel.class);
            Log.e("CreateUserScreenDAC", "onCreate if isProfileEditable then userModel retrieved from preferences is: userId: " + userModel.userId + " userPassword: " + userModel.User.userEncryptedPassword);

            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CreateUserScreenDAC.this.CONNECTIVITY_SERVICE);
            if(CheckInternetConnection(connectivityManager)) {
                processFetchUserDetails();
            }
        } else {

            toolbarTitleTextView.setText(getString(R.string.creatuser_btn));

            initAdapter(null);
        }
    }

    private void initAdapter(UserModel tempUserModel) {

        Log.e("CreateUserScreenDAC", "initAdapter called");

        initValue(tempUserModel);

        footerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isValid = checkValidation();
                if (isValid) {
                    process(userObject);
                }
            }
        });
    }

    private void setToolbar() {

        Log.e("CreateUserScreenDAC", "setToolbar called");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitleTextView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_red);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void initValue(UserModel tempUserModel) {

        Log.e("CreateUserScreenDAC", "initValue called");

        createScreenCellModelArrayList.clear();

        if (tempUserModel == null) {
            if(isProfileEditable) {
                ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenDAC.this, "user_pref", 0);
                userModel = complexPreferences.getObject("current-user", UserModel.class);

                // istop  ismiddle  isbottom hintValue defaultValue
                createScreenCellModelArrayList.add(new CellStandard(true, false, false, textType, getResources().getString(R.string.createuser_hint1), userModel.User.name));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint2), userModel.User.userMail));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint3), userModel.User.userMail));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, passwordType, getResources().getString(R.string.createuser_hint4), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, false, true, passwordType, getResources().getString(R.string.createuser_hint5), ""));

                // isradio  isswitch  gender(1/2) newslater(0/1)
                createScreenCellModelArrayList.add(new CellButton(true, false, userModel.User.userGender, userModel.User.userNewsletter));
                createScreenCellModelArrayList.add(new CellButton(false, true, userModel.User.userGender, userModel.User.userNewsletter));

            }
            else {
                // istop  ismiddle  isbottom hintValue defaultValue
                createScreenCellModelArrayList.add(new CellStandard(true, false, false, textType, getResources().getString(R.string.createuser_hint1), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint2), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint3), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, true, false, passwordType, getResources().getString(R.string.createuser_hint4), ""));
                createScreenCellModelArrayList.add(new CellStandard(false, false, true, passwordType, getResources().getString(R.string.createuser_hint5), ""));

                // isradio  isswitch  gender(1/2) newslater(0/1)
                createScreenCellModelArrayList.add(new CellButton(true, false, 1, 0));
                createScreenCellModelArrayList.add(new CellButton(false, true, 1, 0));
            }
        }
        else {
            // istop  ismiddle  isbottom hintValue defaultValue
            createScreenCellModelArrayList.add(new CellStandard(true, false, false, textType, getResources().getString(R.string.createuser_hint1), tempUserModel.User.name));
            createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint2), tempUserModel.User.userMail));
            createScreenCellModelArrayList.add(new CellStandard(false, true, false, emailType, getResources().getString(R.string.createuser_hint3), tempUserModel.User.userMail));
            createScreenCellModelArrayList.add(new CellStandard(false, true, false, passwordType, getResources().getString(R.string.createuser_hint4), ""));
            createScreenCellModelArrayList.add(new CellStandard(false, false, true, passwordType, getResources().getString(R.string.createuser_hint5), ""));

            // isradio  isswitch  gender(1/2) newslater(0/1)
            createScreenCellModelArrayList.add(new CellButton(true, false, userModel.User.userGender, tempUserModel.User.userNewsletter));
            createScreenCellModelArrayList.add(new CellButton(false, true, userModel.User.userGender, tempUserModel.User.userNewsletter));
        }

        updateAdapter();
    }

    private boolean checkValidation() {

        Log.e("CreateUserScreenDAC", "checkValidation called");

        boolean value = false;
        for (int i = 0; i < listViewCreateUser.getAdapter().getCount() - 1; i++) {

            if (createScreenCellModelArrayList.get(i).isRadioButton()) {
                CellButton cellButton = (CellButton) createScreenCellModelArrayList.get(i);
                userObject.userGender = cellButton.radioValue;
            }
            else if (createScreenCellModelArrayList.get(i).isSwitchButton()) {
                CellButton cellButton = (CellButton) createScreenCellModelArrayList.get(i);
                userObject.userNewsletter = cellButton.switchValue;
            }
            else {
                CellStandard cellStandard = (CellStandard) createScreenCellModelArrayList.get(i);

                if (cellStandard.inputType == textType) {
                    if (Validations.isEmptyField(cellStandard.etValue)) {
                        // TODO proper error handling
                        Toast.makeText(CreateUserScreenDAC.this,getString(R.string.code_create_user_PLEASEENTER) + cellStandard.hintValue, Toast.LENGTH_LONG).show();
                        return false;
                    }
                    else {
                        value = true;
                    }
                }
                else if (cellStandard.inputType == emailType) {
                    if (Validations.isEmptyField(cellStandard.etValue)) {
                        // TODO proper error handling
                        Toast.makeText(CreateUserScreenDAC.this, getString(R.string.code_create_user_PLEASEENTER)  + cellStandard.hintValue, Toast.LENGTH_LONG).show();
                        return false;
                    }
                    else if (!Validations.isEmailPattern(cellStandard.etValue)) {
                        // TODO proper error handling
                        Toast.makeText(CreateUserScreenDAC.this, getString(R.string.code_createuser_ENTEREMAIL), Toast.LENGTH_LONG).show();
                        return false;
                    }
                    else {
                        value = true;
                    }

                    if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.confirm_email)) && cellStandard.etValue.length() != 0) {
                        CellStandard cellTemp = (CellStandard) createScreenCellModelArrayList.get(i - 1);
                        String tempValue = cellTemp.etValue;
                        Log.e("CreateUserScreenDAC", "checkValidation email cell tempValue: " + tempValue + " cellStandard.etValue: " + cellStandard.etValue);
                        if (!Validations.isEmailMatch(cellStandard.etValue, tempValue)) {
                            // TODO proper error handling
                            Toast.makeText(CreateUserScreenDAC.this, getString(R.string.code_EMAILDONOTMATCH), Toast.LENGTH_LONG).show();
                            return false;
                        }
                        else {
                            value = true;
                        }
                    }

                } else if (cellStandard.inputType == passwordType) {
                    if (Validations.isEmptyField(cellStandard.etValue)) {
                        // TODO proper error handling
                        Toast.makeText(CreateUserScreenDAC.this, getString(R.string.code_create_user_PLEASEENTER)+ cellStandard.hintValue, Toast.LENGTH_LONG).show();
                        return false;
                    }
                    else {
                        value = true;
                    }

                    if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.confirm_password)) && cellStandard.etValue.length() != 0) {
                        CellStandard cellTemp = (CellStandard) createScreenCellModelArrayList.get(i - 1);
                        String tempValue = cellTemp.etValue;
                        Log.e("CreateUserScreenDAC", "checkValidation password cell tempValue: " + tempValue + " cellStandard.etValue: " + cellStandard.etValue);
                        if (!Validations.isEmailMatch(cellStandard.etValue, tempValue)) {
                            // TODO proper error handling
                            Toast.makeText(CreateUserScreenDAC.this,getString(R.string.code_PASSWORDDONOTMATCH), Toast.LENGTH_LONG).show();
                            return false;
                        }
                        else {
                            value = true;
                        }
                    }
                }

                if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.name))) {
                    userObject.name = cellStandard.etValue;
                }
                else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.email))) {
                    userObject.userMail = cellStandard.etValue;
                }
                else if (cellStandard.hintValue.equalsIgnoreCase(getResources().getString(R.string.password))) {
                    String encrypted = "";
                    try {
                        encrypted = AeSimpleSHA1.SHA1(cellStandard.etValue.trim());
                    }
                    catch (NoSuchAlgorithmException e) {
                        Log.e("CreateUserScreenDAC", "execption in encryption: " + e.toString());
                        e.printStackTrace();
                    }
                    catch (UnsupportedEncodingException e) {
                        Log.e("CreateUserScreenDAC", "execption in encryption: " + e.toString());
                        e.printStackTrace();
                    }
                    userObject.userEncryptedPassword = encrypted;
                }
            }
        }

        Log.e("CreateUserScreenDAC", "checkValidation returnning: " + value);
        return value;
    }


    private void process(User userObject) {

        Log.e("CreateUserScreenDAC", "process called with userObject: " + userObject.toString());

        try {
            userJSONObject = new JSONObject();
            userJSONObject.put("name", userObject.name.toString().trim());
            userJSONObject.put("mail", userObject.userMail.toString().trim());
            userJSONObject.put("password", userObject.userEncryptedPassword); //sha1 of the password typed in the text edit
            userJSONObject.put("newsletter", String.valueOf(userObject.userNewsletter));
         //   userJSONObject.put("birthday", userObject.userBirthday); //string in format "01-01-1970"
            userJSONObject.put("birthday", "01-01-1970");
            userJSONObject.put("gender", String.valueOf(userObject.userGender));
        }
        catch (Exception e){
            Log.e("CreateUserScreenDAC", "process exception: " + e.toString());
            e.printStackTrace();
        }

        if(isProfileEditable){
            processUpdateUser(userJSONObject,userObject);
        } else {
            processCreateUser(userJSONObject);
        }
    }

    private void processUpdateUser(JSONObject JSONUserObject, User userObject) {

        Log.e("CreateUserScreenDAC", "processUpdateUser called with JSONUserObject: " + JSONUserObject.toString() + " and userObject: " + userObject.toString());

        AppConstants obj= new AppConstants();

        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsDAC.PROJECT_DAC, lang, AppConstantsDAC.CREATE_USER+"/"+userObject.uid);

        RequestJSON json = new RequestJSON();
        json.POST(CreateUserScreenDAC.this, LINK, JSONUserObject.toString());
        json.setPostResponseListener(new POSTResponseListener() {
            @Override
            public String onPost(String msg) {

                Log.e("CreateUserScreenDAC", "onPost response: " + msg);

                try {
                    JSONObject jsonObject = new JSONObject(msg.toString().trim());
                    Iterator<?> keys = jsonObject.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        if (key.toString().trim().equals("error")) {
                            AlertBox.showBox(CreateUserScreenDAC.this, getString(R.string.ERROR), jsonObject.getString("error").toString().trim());
                        }
                        else {
                            UserModel userModelObject = new GsonBuilder().create().fromJson(msg, UserModel.class);
                            /*
                            Log.e("CreateUserScreenDAC", "onPost saving userModelObject in preferences: " + userModelObject.toString());
                            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenDAC.this, "user_pref", 0);
                            complexPreferences.putObject("current-user", userModelObject);
                            complexPreferences.commit();
                            */

                            Log.e("CreateUserScreenDAC", "onPost creating new CongratulationScreenDAC activity");
                            Intent i = new Intent(CreateUserScreenDAC.this, CongratulationScreenDAC.class);
                            i.putExtra("msg", getString(R.string.USER_UPDATE));
                            startActivity(i);
                            finish();
                        }
                    }
                }
                catch (Exception e) {
                    Log.e("CreateUserScreenDAC", "onPost response exception: " + e.toString());
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public void onPreExecute() {

            }

            @Override
            public void onBackground() {

            }
        });
    }


    private void processCreateUser(JSONObject userJSONObject) {

        Log.e("CreateUserScreenDAC", "processCreateUser called with JSONUserObject: " + userJSONObject.toString());

        AppConstants obj= new AppConstants();
        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsDAC.PROJECT_DAC, lang, AppConstantsDAC.CREATE_USER);

        RequestJSON json = new RequestJSON();
        json.POST(CreateUserScreenDAC.this, LINK, userJSONObject.toString());
        json.setPostResponseListener(new POSTResponseListener() {
            @Override
            public String onPost(String msg) {

                Log.e("CreateUserScreenDAC", "onPost response: " + msg);

                try {
                    JSONObject jsonObject = new JSONObject(msg.toString().trim());
                    Iterator<?> keys = jsonObject.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        if (key.toString().trim().equals("error")) {
                            AlertBox.showBox(CreateUserScreenDAC.this, getString(R.string.ERROR), jsonObject.getString("error").toString().trim());
                        } else {
                            UserModel userModelObject = new GsonBuilder().create().fromJson(msg, UserModel.class);
                            Log.e("CreateUserScreenDAC", "onPost saving userModelObject in preferences: " + userModelObject.toString());
                            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenDAC.this, "user_pref", 0);
                            complexPreferences.putObject("current-user", userModelObject);
                            complexPreferences.commit();

                            Log.e("CreateUserScreenDAC", "onPost saving isUserLogin in preferences: " + true);
                            SharedPreferences preferences = getSharedPreferences("login", MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean("isUserLogin", true);
                            editor.commit();

                            Log.e("CreateUserScreenDAC", "onPost creating new CongratulationScreenDAC activity");
                            Intent i = new Intent(CreateUserScreenDAC.this, CongratulationScreenDAC.class);
                            i.putExtra("msg", getString(R.string.congo_msg));
                            startActivity(i);
                            finish();
                        }
                    }
                }
                catch (Exception e) {
                    Log.e("CreateUserScreenDAC", "onPost response exception: " + e.toString());
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public void onPreExecute() {

            }

            @Override
            public void onBackground() {

            }
        });

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_createuserscreen;
    }

    private void processFetchUserDetails() {

        Log.e("CreateUserScreenDAC", "processFetchUserDetails called");

        dialog = new HUD(CreateUserScreenDAC.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.title("");
        dialog.show();

        RequestJSON json = new RequestJSON();
        AppConstants obj = new AppConstants();
        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");


        String LINK = obj.setURL(AppConstantsDAC.PROJECT_DAC, lang, AppConstantsDAC.CREATE_USER+"/"+userModel.userId);
        json.GET_METHOD(LINK);

        json.setResponseListener(new ResponseListener() {
            @Override
            public String response(String response) {

                Log.e("CreateUserScreenDAC", "onPost response: " + response);

                dialog.dismiss();
                UserModel tempUserModel = new GsonBuilder().create().fromJson(response, UserModel.class);
                /*
                Log.e("CreateUserScreenDAC", "onPost saving userModelObject in preferences: " + userModel.toString());
                ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(CreateUserScreenDAC.this, "user_pref", 0);
                complexPreferences.putObject("current-user", userModel);
                complexPreferences.commit();
                */

                initAdapter(tempUserModel);

                return null;
            }

            @Override
            public String error(String error) {
                Log.e("CreateUserScreenDAC", "GET response exception: " + error);
                return error;
            }
        });
    }
}