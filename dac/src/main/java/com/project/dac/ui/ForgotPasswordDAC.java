package com.project.dac.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.project.dac.R;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.interfaces.POSTResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.ui.ForgotPasswordScreen;
import com.project.client_library.widget.AlertBox;

import org.json.JSONObject;

import java.util.Iterator;

import helpers.AppConstantsDAC;


public class ForgotPasswordDAC extends ForgotPasswordScreen {
    private Toolbar toolbar;
    private EditText etEmail;
    private Button btnSend;

    private JSONObject userObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_red);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        toolbar_Title.setText(getString(R.string.FORGOT_PASSWORD));


        etEmail = (EditText)findViewById(R.id.etEmail);
        btnSend = (Button)findViewById(R.id.btnSend);


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if(isEmptyField(etEmail)){
                  Toast.makeText(ForgotPasswordDAC.this, getString(R.string.ENTER_EMAIL), Toast.LENGTH_LONG).show();
              }else{
                    processForgotPassword();
              }

            }
        });
    }


    private void processForgotPassword(){
        try {

            userObject = new JSONObject();
            userObject.put("mail", etEmail.getText().toString().trim());


        }catch (Exception e){
            Log.e("exc", e.toString());
        }
        processResponse();
    }


    private void processResponse(){

        AppConstants obj= new AppConstants();
        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsDAC.PROJECT_DAC,lang,AppConstantsDAC.FORGOT_PASSWORD);

        RequestJSON json = new RequestJSON();
        json.POST(ForgotPasswordDAC.this, LINK, userObject.toString());

        json.setPostResponseListener(new POSTResponseListener() {
            @Override
            public String onPost(final String msg) {
                Log.e("main response", msg);
                try {

                    JSONObject jobj = new JSONObject(msg.toString().trim());
                    Iterator<?> keys = jobj.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();

                        if (key.toString().trim().equals("error")) {
                            AlertBox.showBox(ForgotPasswordDAC.this, "Login", jobj.getString("error").toString().trim());
                        } else {

                            Intent i = new Intent(ForgotPasswordDAC.this, CongratulationScreenDAC.class);
                            i.putExtra("status", 1);
                            i.putExtra("msg", getString(R.string.PLS_CHK_EMAIL));
                            startActivity(i);
                            finish();
                        }

                    }

                } catch (Exception e) {
                    Log.e("excepp", e.toString());
                }
                return null;
            }

            @Override
            public void onPreExecute() {

            }

            @Override
            public void onBackground() {

            }
        });

    }


    private boolean isEmptyField(EditText param1) {
        boolean isEmpty = false;
        if (param1.getText() == null || param1.getText().toString().equalsIgnoreCase("")) {
            isEmpty = true;
        }
        return isEmpty;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_forgotpassword;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }



    //end of main class
}
