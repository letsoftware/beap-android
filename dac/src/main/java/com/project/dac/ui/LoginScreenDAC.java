package com.project.dac.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.project.client_library.helpers.AeSimpleSHA1;
import com.project.dac.R;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.POSTResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.UserModel;
import com.project.client_library.ui.LoginScreen;
import com.project.client_library.widget.AlertBox;
import com.project.client_library.widget.HUD;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import helpers.AppConstantsDAC;


public class LoginScreenDAC extends LoginScreen implements View.OnClickListener{
    private Toolbar toolbar;
    private TextView txtCreateUser,txtForgotPass;
    private Button btnLogin;
    private EditText etEmail,etPassword;
    private String encryptedPassword,decryptedPassword;
    private HUD dialog;
    private JSONObject userObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_red);
        toolbar_Title.setText(getString(R.string.LOGIN));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        intView();
    }


    private void intView() {
        txtCreateUser = (TextView)findViewById(R.id.txtCreateUser);
        txtForgotPass = (TextView)findViewById(R.id.txtForgotPass);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);

        btnLogin.setOnClickListener(this);
        txtCreateUser.setOnClickListener(this);
        txtForgotPass.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void processPasswordEncryption() {
        new AsyncTask<Void,Void,Void>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new HUD(LoginScreenDAC.this,android.R.style.Theme_Translucent_NoTitleBar);
                dialog.title("");
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    encryptedPassword = AeSimpleSHA1.SHA1(etPassword.getText().toString().trim());
                }
                catch (NoSuchAlgorithmException e) {
                    Log.e("LoginScreenDAC", "processPasswordEncryption exception: " + e.toString());
                    e.printStackTrace();
                }
                catch (UnsupportedEncodingException e) {
                    Log.e("LoginScreenDAC", "processPasswordEncryption exception: " + e.toString());
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                dialog.dismiss();
                processLogin();
            }
        }.execute();
    }

    private void processLogin() {
        try {
            userObject = new JSONObject();
            userObject.put("mail", etEmail.getText().toString().trim());
            userObject.put("password", encryptedPassword); //sha1 of the password typed in the text edit
        }
        catch (Exception e){
            Log.e("LoginScreenDAC", "processLogin exception: " + e.toString());
            e.printStackTrace();
        }

        processResponse();
    }

    private void processResponse(){

        AppConstants obj= new AppConstants();
        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsDAC.PROJECT_DAC, lang, AppConstantsDAC.LOGIN);

        RequestJSON json = new RequestJSON();

        Log.e("LoginScreenDAC", "processResponse POST login " + LINK + " with userObject: " + userObject.toString());
        json.POST(LoginScreenDAC.this, LINK, userObject.toString());

        json.setPostResponseListener(new POSTResponseListener() {

            @Override
            public String onPost(final String msg) {

                Log.e("LoginScreenDAC", "processResponse called");

                try {

                    JSONObject jobj = new JSONObject(msg.toString().trim());
                    Iterator<?> keys = jobj.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();

                        if (key.toString().trim().equals("error")) {

                            AlertBox.showBox(LoginScreenDAC.this, getString(R.string.LOGIN), jobj.getString("error").toString().trim());

                        } else {

                            UserModel userModelObject = new GsonBuilder().create().fromJson(msg, UserModel.class);
                            userModelObject.User.userEncryptedPassword = encryptedPassword;
                            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(LoginScreenDAC.this, "user_pref", 0);
                            complexPreferences.putObject("current-user", userModelObject);
                            complexPreferences.commit();
                            Log.e("LoginScreenDAC", "processResponse save UserModel in preferences, userId: " + userModelObject.userId + " userPassword: " + userModelObject.User.userEncryptedPassword);

                            SharedPreferences preferences = getSharedPreferences("login", MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean("isUserLogin", true);
                            // editor.putString("userId ",edLoginPassword.getText().toString().trim());
                            editor.commit();

                            Intent i = new Intent(LoginScreenDAC.this, HomeScreenDAC.class);
                            startActivity(i);
                            finish();
                        }

                    }

                }
                catch (Exception e) {
                    Log.e("LoginScreenDAC", "process exception: " + e.toString());
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public void onPreExecute() {

            }

            @Override
            public void onBackground() {

            }
        });

    }









    @Override
    public void onClick(View v) {

        Log.e("LoginScreenDAC", "onClick called");

        switch (v.getId()){
            case R.id.txtCreateUser:
                Intent i = new Intent(LoginScreenDAC.this, CreateUserScreenDAC.class);
                startActivity(i);
                break;
            case R.id.txtForgotPass:
                Intent i2 = new Intent(LoginScreenDAC.this, ForgotPasswordDAC.class);
                startActivity(i2);
                break;
            case R.id.btnLogin:
                if (isEmptyField(etEmail)) {
                    Toast.makeText(LoginScreenDAC.this, "Please Enter Email Address", Toast.LENGTH_LONG).show();
                }else if (isEmptyField(etEmail))  {
                    Toast.makeText(LoginScreenDAC.this,"Please Enter Password",Toast.LENGTH_LONG).show();
                }else{
                    processPasswordEncryption();
                }
               break;
        }
    }

    private boolean isEmptyField(EditText param1) {
        boolean isEmpty = false;
        if (param1.getText() == null || param1.getText().toString().equalsIgnoreCase("")) {
            isEmpty = true;
        }
        return isEmpty;
    }

    //end of main class
}
