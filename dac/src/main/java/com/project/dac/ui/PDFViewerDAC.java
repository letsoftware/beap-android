package com.project.dac.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.project.dac.R;
import com.project.client_library.widget.HUD;


public class PDFViewerDAC extends ActionBarActivity {


    private HUD dialog;
    private WebView webview;
//    private com.joanzapata.pdfview.PDFView PDFVIEW;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdf_viewer);
        webview = (WebView)findViewById(R.id.webView);

        SharedPreferences preferences = getSharedPreferences("lock", MODE_PRIVATE);
        if(preferences.getBoolean("isLock",false)){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }


    //   PDFVIEW = (com.joanzapata.pdfview.PDFView)findViewById(R.id.pdfview);


        String pdfPath = getIntent().getStringExtra("url");


        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);

        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        dialog = new HUD(PDFViewerDAC.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.title(getString(R.string.LOADING));
        dialog.show();
        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e("Error:",""+description);

            }
        });

        webview.loadUrl("http://docs.google.com/gview?embedded=true&url=" + pdfPath);




    }



    //end of main class
}
