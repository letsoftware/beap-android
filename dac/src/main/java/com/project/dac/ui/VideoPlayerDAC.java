package com.project.dac.ui;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import com.project.dac.R;
import com.project.client_library.widget.HUD;

import org.json.JSONObject;


public class VideoPlayerDAC extends ActionBarActivity {

    private VideoView videoPlayer;
    private HUD dialog;
    private JSONObject userObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player);

        videoPlayer = (VideoView) findViewById(R.id.videoPlayer);

        SharedPreferences preferences = getSharedPreferences("lock", MODE_PRIVATE);
        if(preferences.getBoolean("isLock",false)){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }


        String path = getIntent().getStringExtra("url");

        dialog = new HUD(VideoPlayerDAC.this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.title(getString(R.string.BUFFERING));
        dialog.show();


        try {
            MediaController mc = new MediaController(VideoPlayerDAC.this);
            mc.setAnchorView(videoPlayer);
            mc.setMediaPlayer(videoPlayer);
            Uri uri = Uri.parse(path);
            videoPlayer.setMediaController(mc);
            videoPlayer.setVideoURI(uri);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }


        videoPlayer.requestFocus();
        videoPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                dialog.dismiss();
                videoPlayer.start();
            }
        });






    }


    //end of main class
}
