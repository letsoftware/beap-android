package com.project.dac.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.project.client_library.ui.WelcomeScreen;
import com.project.dac.R;


public class WelcomeScreenDAC extends WelcomeScreen {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {

            case R.id.btnCreateUser:
                Intent i = new Intent(WelcomeScreenDAC.this, CreateUserScreenDAC.class);
                startActivity(i);

                break;

            case R.id.btnLogin:
                Intent i2 = new Intent(WelcomeScreenDAC.this, LoginScreenDAC.class);
                startActivity(i2);

                break;
        }
    }
}
