package fragments;

import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;
import com.project.dac.R;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.ItemModel;
import com.project.client_library.widget.HUD;

import helpers.AppConstantsDAC;

import static com.project.client_library.helpers.CheckInternet.CheckInternetConnection;


public class AboutUsFragment extends Fragment {
    private ItemModel aboutUsObject;
    private ImageView imgAboutUs;
    private TextView txtTitle,txtDescription;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private HUD dialog;

    public static AboutUsFragment newInstance(String param1, String param2) {
        AboutUsFragment fragment = new AboutUsFragment();

        return fragment;
    }

    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView= inflater.inflate(R.layout.fragment_about_us, container, false);

        imgAboutUs= (ImageView) convertView.findViewById(R.id.imgAboutUs);
        txtTitle= (TextView) convertView.findViewById(R.id.txtTitle);
        txtDescription= (TextView) convertView.findViewById(R.id.txtDescription);
        toolbar= (Toolbar) convertView.findViewById(R.id.toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);


        return  convertView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
        if(CheckInternetConnection(connectivityManager)){

            processfetchABOUTUS();
        } else {
            txtTitle.setText(aboutUsObject.title+"");
            txtDescription.setText(aboutUsObject.content+"");
            Glide.with(getActivity()).load(aboutUsObject.thumbnail).thumbnail(0.1f).into(imgAboutUs);
            toolbarTitle.setText(aboutUsObject.title + "");
        }

    }

    private void processfetchABOUTUS() {
        dialog = new HUD(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);
        dialog.title("");
        dialog.show();

        RequestJSON json = new RequestJSON();

        AppConstants obj= new AppConstants();

        SharedPreferences preferences = getActivity().getSharedPreferences("language", getActivity().MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");


        String LINK = obj.setURL(AppConstantsDAC.PROJECT_DAC,lang,AppConstantsDAC.ABOUT_INFO);

        json.GET_METHOD(LINK);

        json.setResponseListener(new ResponseListener() {
            @Override
            public String response(String response) {
                dialog.dismiss();
                aboutUsObject = new GsonBuilder().create().fromJson(response, ItemModel.class);
                ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "about_pref", 0);
                complexPreferences.putObject("about_us", aboutUsObject);
                complexPreferences.commit();

                txtTitle.setText(aboutUsObject.title + "");
                txtDescription.setText(aboutUsObject.content + "");
                Glide.with(getActivity()).load(aboutUsObject.thumbnail).thumbnail(0.1f).into(imgAboutUs);
                toolbarTitle.setText(aboutUsObject.title + "");
                return null;
            }

            @Override
            public String error(String error) {
                Log.e("excp err on welcome", error);
                dialog.dismiss();
                return null;
            }
        });
    }
}
