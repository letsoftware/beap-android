package fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.project.dac.R;
import com.project.client_library.adapters.ImageViewPagerAdapter;
import com.project.client_library.circle_indicator.CirclePageIndicator;
import com.project.client_library.circle_indicator.PageIndicator;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.HeadlinesStructureModel;
import com.project.client_library.widget.HUD;

import helpers.AppConstantsDAC;


public class HomeFragment extends Fragment implements View.OnClickListener{
    private HUD dialog;
    private Toolbar toolbar;
    private TextView txtImageTitle2,txtImageTitle1;
    private ImageView imgProducts,imgTours;
    private ViewPager viewPager;

    private PageIndicator mIndicator;
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertview =  inflater.inflate(R.layout.fragment_home, container, false);

        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_Title.setText("DAC.");

//        txtImageTitle1 = (TextView) convertview.findViewById(R.id.txtImageTitle1);
//        txtImageTitle2 = (TextView) convertview.findViewById(R.id.txtImageTitle2);
        imgProducts= (ImageView) convertview.findViewById(R.id.imgProducts);
        imgTours= (ImageView) convertview.findViewById(R.id.imgTours);
        viewPager = (ViewPager)convertview. findViewById(R.id.imagePager);
        mIndicator = (CirclePageIndicator)convertview.findViewById(R.id.guideIndicator);


        processfetchdata();

        imgProducts.setOnClickListener(this);
        imgTours.setOnClickListener(this);



        return convertview;
    }



    private void processfetchdata(){

        dialog = new HUD(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);
        dialog.title("");
        dialog.show();


        RequestJSON json = new RequestJSON();

        AppConstants obj= new AppConstants();

        SharedPreferences preferences = getActivity().getSharedPreferences("language", getActivity().MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsDAC.PROJECT_DAC,lang,AppConstantsDAC.HEADLINES);

        json.GET_METHOD(LINK);

        json.setResponseListener(new ResponseListener() {
            @Override
            public String response(String response) {

                HeadlinesStructureModel headlineObj = new GsonBuilder().create().fromJson(response, HeadlinesStructureModel.class);

                ImageViewPagerAdapter adapter = new ImageViewPagerAdapter(getActivity(), headlineObj);
                viewPager.setAdapter(adapter);
                mIndicator.setViewPager(viewPager);

                dialog.dismiss();


                return null;
            }

            @Override
            public String error(String error) {
                Log.e("excp err on welcome", error);
                dialog.dismiss();
                return null;
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgProducts:

                Bundle bundle = new Bundle();
                bundle.putString("frag-name", getString(R.string.EXHBITIONS));
                CategoryViewFragment catg = new CategoryViewFragment();
                FragmentManager fragmentManager = getFragmentManager();

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.realtabcontent, catg,"CATG");
                catg.setArguments(bundle);

                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                break;
            case R.id.imgTours:


                Bundle bundle2 = new Bundle();
                bundle2.putString("frag-name", getString(R.string.WWW));
                CategoryViewDetailsFragment catg2 = new CategoryViewDetailsFragment();
                FragmentManager fragmentManager2 = getFragmentManager();

                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                fragmentTransaction2.replace(R.id.realtabcontent, catg2);
                catg2.setArguments(bundle2);

                fragmentTransaction2.addToBackStack(null);
                fragmentTransaction2.commit();
                break;
        }
    }
}
