package fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.project.dac.R;
import com.project.dac.ui.PDFViewerDAC;
import com.project.dac.ui.VideoPlayerDAC;
import com.project.client_library.adapters.ProductImageViewPagerAdapter;
import com.project.client_library.circle_indicator.CirclePageIndicator;
import com.project.client_library.circle_indicator.PageIndicator;
import com.project.client_library.helpers.AppConstants;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.POSTResponseListener;
import com.project.client_library.interfaces.ResponseListener;
import com.project.client_library.jsonParsing.RequestJSON;
import com.project.client_library.model.ProductModel;
import com.project.client_library.model.UserModel;
import com.project.client_library.widget.HUD;

import org.json.JSONArray;
import org.json.JSONObject;

import helpers.AppConstantsDAC;

public class ItemsDetailsFragment extends Fragment implements View.OnClickListener{
    private ViewPager view;
    private Toolbar toolbar;
   private TextView textDistance,title,subtitle,desc;
    private boolean isFavAdded;


    private TextView txtdata,txtMovie,txtShare,txtFav;
    private int main_pos,p_id;
    private HUD dialog;
    private ProductModel prod_model;
    private ViewPager viewPager;
    private PageIndicator mIndicator;
    private ImageView imgFav,imgData,imgVideo,imgShare;
    private boolean isData,isVideo;
    UserModel userObj;
    JSONArray jarray;


    public static ItemsDetailsFragment newInstance(String param1, String param2) {
        ItemsDetailsFragment fragment = new ItemsDetailsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public ItemsDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View convertview = inflater.inflate(R.layout.fragment_item_details, container, false);

        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_red);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        title = (TextView)convertview.findViewById(R.id.txtTitle);
        subtitle = (TextView)convertview.findViewById(R.id.txtSubTitle);
        desc = (TextView)convertview.findViewById(R.id.txtDescription);



        txtdata = (TextView)convertview.findViewById(R.id.txtdata);
        txtMovie = (TextView)convertview.findViewById(R.id.txtMovie);
        txtShare = (TextView)convertview.findViewById(R.id.txtShare);
        txtFav = (TextView)convertview.findViewById(R.id.txtFav);


        textDistance = (TextView)convertview.findViewById(R.id.textDistance);
        imgFav = (ImageView)convertview.findViewById(R.id.imgFav);
        imgData = (ImageView)convertview.findViewById(R.id.imgData);
        imgVideo = (ImageView)convertview.findViewById(R.id.imgVideo);
        imgShare = (ImageView)convertview.findViewById(R.id.imgShare);

        viewPager = (ViewPager)convertview. findViewById(R.id.imagePager);
        mIndicator = (CirclePageIndicator)convertview.findViewById(R.id.pageIndicator);

        Bundle getbundle = this.getArguments();
        toolbar_Title.setText(getbundle.getString("toolbar-title"));

       /* title.setText(getbundle.getString("frag-title"));
        subtitle.setText(getbundle.getString("frag-subtitle"));
        main_pos = getbundle.getInt("mainpos");
     */
        p_id = getbundle.getInt("id");


        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "user_pref", 0);
        userObj = complexPreferences.getObject("current-user", UserModel.class);



        processfetchData(p_id);


        imgFav.setOnClickListener(this);
        imgShare.setOnClickListener(this);
        imgData.setOnClickListener(this);
        imgVideo.setOnClickListener(this);





        return convertview;
    }




private void processfetchData(int prod_id){
    dialog = new HUD(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);
    dialog.title("");
    dialog.show();


    RequestJSON json = new RequestJSON();

    AppConstants obj= new AppConstants();

    SharedPreferences preferences = getActivity().getSharedPreferences("language", getActivity().MODE_PRIVATE);
    String lang = preferences.getString("lang", "en");

    String LINK = obj.setURL(AppConstantsDAC.PROJECT_DAC,lang,AppConstantsDAC.ITEM_DETAILS+prod_id);

    json.GET_METHOD(LINK);

    json.setResponseListener(new ResponseListener() {
        @Override
        public String response(String response) {

            Log.e("response-->", response);
            prod_model = new GsonBuilder().create().fromJson(response, ProductModel.class);
            initalizeviewpagerAdapter(response);


            processfetchFavouritesItems(p_id);

            return null;
        }

        @Override
        public String error(String error) {
            dialog.dismiss();
            Log.e("excp err on welcome", error);
            return null;
        }
    });

}


    private void initalizeviewpagerAdapter(String response){

        ProductImageViewPagerAdapter adapter = new ProductImageViewPagerAdapter(getActivity(),prod_model);
        viewPager.setAdapter(adapter);
        mIndicator.setViewPager(viewPager);


         title.setText(prod_model.title);
         subtitle.setText(prod_model.subtitle);
         desc.setText(prod_model.content);

        try {
            JSONObject jobj = new JSONObject(response);

            if(jobj.has("files")){
                isData = true;
            }else{
                isData = false;


            }

            if(jobj.has("videos")){
                isVideo = true;
            }else{
                isVideo = false;

            }

        }catch (Exception e){
            Log.e("exc",e.toString());
        }



        processsetImageButtons();


    }



private void processsetImageButtons(){

    if(isData){
        imgData.setEnabled(true);

        int col = Color.parseColor("#ffffff");
        imgData.setColorFilter(col, PorterDuff.Mode.SRC_ATOP);
        imgData.setImageResource(R.drawable.buttonpanelseedata);

    }else{

        imgData.setEnabled(false);
        txtdata.setTextColor(Color.parseColor("#A8AEB2"));
        int col = Color.parseColor("#A8AEB2");
        imgData.setColorFilter(col, PorterDuff.Mode.SRC_ATOP);
        imgData.setImageResource(R.drawable.buttonpanelseedata);
    }



    if(isVideo){
       imgVideo.setEnabled(true);

        int col = Color.parseColor("#ffffff");
        imgVideo.setColorFilter(col, PorterDuff.Mode.SRC_ATOP);
        imgVideo.setImageResource(R.drawable.buttonpanelwatchmovie);

    }else{

        imgVideo.setEnabled(false);
        txtMovie.setTextColor(Color.parseColor("#A8AEB2"));
        int col = Color.parseColor("#A8AEB2");
        imgVideo.setColorFilter(col, PorterDuff.Mode.SRC_ATOP);
        imgVideo.setImageResource(R.drawable.buttonpanelwatchmovie);
    }
}

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.imgFav:

                if(isFavAdded)
                processCallJSON(true);
                else
                    processCallJSON(false);

                break;

            case R.id.imgData:
                showPDF(prod_model.files.get(0).toString().trim());

                break;
            case R.id.imgVideo:
                playVideo(prod_model.videos.get(0).toString().trim());
                break;
            case R.id.imgShare:
                Toast.makeText(getActivity(),"Comming Soon",Toast.LENGTH_SHORT).show();
                break;
        }
    }


    private void processfetchFavouritesItems(final int pid){

        RequestJSON json = new RequestJSON();

        AppConstants obj= new AppConstants();
        SharedPreferences preferences = getActivity().getSharedPreferences("language", getActivity().MODE_PRIVATE);
        String lang = preferences.getString("lang", "en");

        String LINK = obj.setURL(AppConstantsDAC.PROJECT_DAC,lang,AppConstantsDAC.FAVOURITES+userObj.userId);

        json.GET_METHOD(LINK);

        json.setResponseListener(new ResponseListener() {
            @Override
            public String response(String response) {
                dialog.dismiss();

                Log.e("response fav-->", response);

                try {
                    JSONObject newobj = new JSONObject(response);
                    jarray = new JSONArray(newobj.getJSONArray("favourites").toString());

                    processCheckFavItems(pid);


                } catch (Exception e) {
                    Log.e("exc", e.toString());
                }


                return null;
            }

            @Override
            public String error(String error) {
                dialog.dismiss();
                Log.e("excp err on welcome", error);
                return null;
            }
        });

    }




private void processCheckFavItems(int pid){
try {
    for (int i = 0; i < jarray.length(); i++) {
        if (jarray.get(i).equals(String.valueOf(pid))) {
            isFavAdded =true;
        }
    }

    if(isFavAdded){
        ItemAddedToFavourite();
    }else{
        ItemRemoveToFavourite();
    }

}catch (Exception e){
    Log.e("exc in conversion",e.toString());
}

}

private void ItemAddedToFavourite(){
    imgFav.setImageResource(R.drawable.buttonpanelfavoritfill);
    isFavAdded = true;
}

private void ItemRemoveToFavourite(){
    imgFav.setImageResource(R.drawable.buttonpanelfavorit);
    isFavAdded = false;

 }




private void processCallJSON(final boolean isfavadded){

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "user_pref", 0);
        UserModel userObj = complexPreferences.getObject("current-user", UserModel.class);
        Log.e("uidd",""+userObj.User.uid);
        Log.e("uidd2",String.valueOf(userObj.userId));


        try{




            JSONObject favObj = new JSONObject();

            favObj.put("uid", userObj.User.uid);
            favObj.put("id", p_id);

           if(isfavadded)
            favObj.put("remove",1);



            AppConstants obj= new AppConstants();
            SharedPreferences preferences = getActivity().getSharedPreferences("language", getActivity().MODE_PRIVATE);
            String lang = preferences.getString("lang", "en");

            String LINK = obj.setURL(AppConstantsDAC.PROJECT_DAC,lang,AppConstantsDAC.FAVOURITES);

            RequestJSON json = new RequestJSON();
            json.POST(getActivity(), LINK, favObj.toString());

            json.setPostResponseListener(new POSTResponseListener() {
                @Override
                public String onPost(final String msg) {
                    Log.e("main response", msg);

                    if (isfavadded) {
                        ItemRemoveToFavourite();
                    } else {
                        ItemAddedToFavourite();

                    }

                    return null;
                }

                @Override
                public void onPreExecute() {

                }

                @Override
                public void onBackground() {

                }
            });




        }catch (Exception e){
            Log.e("exc",e.toString());
        }
    }


    private void showPDF(String PDFURL){


       /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PDFURL));
        startActivity(browserIntent);*/

        Intent intent = new Intent(getActivity(), PDFViewerDAC.class);
        intent.putExtra("url", PDFURL);
        startActivity(intent);
    }


    private void playVideo(String VIDEOURL){
        Intent i = new Intent(getActivity(), VideoPlayerDAC.class);
        i.putExtra("url",VIDEOURL);
        startActivity(i);
    }


//end of main class
}
