package fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.project.dac.R;
import com.project.client_library.adapters.ItemviewCollectionAdapter;
import com.project.client_library.helpers.ComplexPreferences;
import com.project.client_library.interfaces.ButtonClickListner;
import com.project.client_library.interfaces.SettingCell;
import com.project.client_library.model.CategoryStructureModel;

import java.util.ArrayList;

public class ItemscollectionviewFragment extends Fragment implements ButtonClickListner {
    private ListView listView_Settings;
    private Toolbar toolbar;
    private ArrayList<SettingCell> settingsitems;
    public ViewPager view;
    CategoryStructureModel  catgobj;
    boolean isSubCatg;

    public static ItemscollectionviewFragment newInstance(String param1, String param2) {
        ItemscollectionviewFragment fragment = new ItemscollectionviewFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public ItemscollectionviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View convertview = inflater.inflate(R.layout.items_collection_view, container, false);

        toolbar = (Toolbar) convertview.findViewById(R.id.toolbar);
        TextView toolbar_Title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.icon_back_red);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        Bundle getbundle = this.getArguments();
        String title = getbundle.getString("frag-name");

        final int main_pos = getbundle.getInt("pos");
        final int sub_pos = getbundle.getInt("sub_pos",0);
        isSubCatg = getbundle.getBoolean("issubCatg");

        toolbar_Title.setText(title);


        view  = (ViewPager)convertview.findViewById(R.id.view);
        view.setPageMargin(-50);

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "user_pref", 0);
        catgobj = complexPreferences.getObject("catg", CategoryStructureModel.class);

        ItemviewCollectionAdapter adapter = new ItemviewCollectionAdapter(getActivity(),catgobj,main_pos,sub_pos,isSubCatg);
        view.setAdapter(adapter);
        view.setCurrentItem(0);



        adapter.setButtonlistener(new ButtonClickListner() {

            @Override
            public void ButtonClick(int pos) {

                Bundle bundle = new Bundle();


                if(!isSubCatg) {
                    bundle.putInt("id", catgobj.categories.get(main_pos).items.get(pos).id);
                    bundle.putString("toolbar-title",catgobj.categories.get(main_pos).items.get(pos).title);
                }
                else {
                    bundle.putInt("id", catgobj.categories.get(main_pos).subcategory.get(sub_pos).items.get(pos).id);
                    bundle.putString("toolbar-title", catgobj.categories.get(main_pos).subcategory.get(sub_pos).name);
                }

                ItemsDetailsFragment item = new ItemsDetailsFragment();
                FragmentManager fragmentManager1 = getFragmentManager();

                FragmentTransaction fragmentTransaction1 = fragmentManager1.beginTransaction();
                fragmentTransaction1.replace(R.id.realtabcontent, item,"CATG");
                item.setArguments(bundle);

                fragmentTransaction1.addToBackStack(null);
                fragmentTransaction1.commit();
            }
        });



        return convertview;
    }





    @Override
    public void ButtonClick(int pos) {

    }
}
