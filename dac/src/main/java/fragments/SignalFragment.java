package fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.dac.R;
import com.project.client_library.model.AboutUs;
import com.project.client_library.widget.HUD;


public class SignalFragment extends Fragment {
    private  AboutUs aboutUsObject;
    private ImageView imgAboutUs;
    private TextView txtTitle,txtDescription;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private HUD dialog;

    public static SignalFragment newInstance(String param1, String param2) {
        SignalFragment fragment = new SignalFragment();

        return fragment;
    }

    public SignalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView= inflater.inflate(R.layout.fragment_signal, container, false);




        return  convertView;
    }


}
